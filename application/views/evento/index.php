<div class="ui container">
  <p>&nbsp;</p>
  <h1 class="ui dividing teal header">Cursos disponibles</h1>
  <div class ="ui segment">
    <p>
      Bienvenid@ al sistema de matrículas del Centro de Capacitación José Pedro Varela.
      A continuación encontrará los cursos para disponibles para matricularse.<br />
    </p>

    <div class="ui link cards">
      <?php foreach ($eventos as $evento):?>
        <div class="card">
          <div class="image">
            <div class="ui pink right ribbon label">
              <i class="dollar sign icon"></i> <?php echo number_format($this->evento_model->obtenerCostos($evento['idEvento'])[0]['costoValorEvento'], 2); ?>
            </div>
            <?php if ($evento['imagenEvento']!= null):?>
            <img src="<?php echo base_url('/application/cargasDocumentos/imagenes/'.$evento['imagenEvento'])?>">
            <?php else: ?>
              <img src="<?php echo base_url('/application/assets/images/imagen_no_disponible.png')?>">
            <?php endif;?>
          </div>
          <div class="content">
            <div class="header"><?php echo $evento['tituloEvento'] ?></div>
          </div>
          <div class="description" style="padding-left:2em">
            <?php
              $fechaInicioEvento = new DateTime($evento['fechaInicioEvento']);
              $fechaInicioEvento = new Carbon\Carbon($fechaInicioEvento->format(DATE_ISO8601));
              $fechaFinEvento = new DateTime($evento['fechaFinEvento']);
              $fechaFinEvento = new Carbon\Carbon($fechaFinEvento->format(DATE_ISO8601));
             ?>
            <p>
              Inicia el <?php echo $fechaInicioEvento->formatLocalized('%A, %d de %B de %Y');?><br />
              Termina el <?php echo $fechaFinEvento->formatLocalized('%A, %d de %B de %Y');?>
            </p>
          </div>
          <div class="extra content">
            <span class="right floated">
              <a class="ui teal basic button" href="<?php echo base_url('evento/'.$evento['idEvento']); ?>">Más Información</a>
            </span>
          </div>
        </div>
      <?php endforeach; ?>
    </div>

  </div>

</div>
