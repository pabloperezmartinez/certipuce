<?php
/**
 * Login para participantes
 */
?>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="icon" href="<?php echo base_url("application/assets/img/favicon.ico.png")?>" type="image/png">
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery-2.1.3.min.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/semantic.css') ?>">
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script src="<?php echo base_url('application/assets/semantic-ui/semantic.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/functions.js') ?>"></script>
		<link rel="stylesheet/less" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/src/definitions/collections/menu.less') ?>">
		<link rel="stylesheet" href="<?php echo base_url('application/assets/custom.css') ?>" type="text/css">
		<title>Sistema de Matrícula</title>
	</head>
	<body>
		<div class="ui text container margen">
			<img class="ui centered small image" src="<?php echo base_url('application/assets/images/ccjpv-white.png')?>"/>
			<br>
			<div class="ui one column center aligned grid">
				<div class="column">
				<h2 class="ui center aligned header content">
					Sistema de Matríclas
				</h2><br>
				<p>Recuperación de Contraseña</p>
				<form action="<?php echo base_url('/participante/recuperarContrasena')?>" method="post" class="ui large form">
					<div class="ui stacked segment">
						<div class="field" id="email">
							<div class="ui left icon input">
								<i class="mail icon"></i>
								<input type="text" name="email" placeholder="Correo Electrónico">
								<input type="hidden" name="idEvento" id="idEvento" value="<?php echo $this->session->flashdata('idEvento');?>">
							</div>
						</div>
						<input type="submit" value="Ingresar" class="ui teal button">
					</div>
				</form>
				</div>
				<div class="column">
					<a href="<?php echo base_url('/evento/'.$this->session->flashdata('idEvento'))?>">Regresar</a>
				</div>
			</div>
		</div>
	</body>
</html>

<script>
$('.ui.form')
.form({
  fields: {
    name: {
      identifier: 'email',
      rules: [
        {
          type   : 'email',
          prompt : 'Por favor ingrese un email válido'
        }
      ]
    }
  },
  inline:true
})
;
</script>
