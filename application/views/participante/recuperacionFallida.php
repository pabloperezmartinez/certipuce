<div class="ui segment container" style="min-height: 80%">
	<span style="float:right;">
		<a href="<?php echo base_url('/evento/'.$this->session->flashdata('idEvento'))?>"><i class="circular inverted large reply icon"></i> Regresar</a>
	</span>
	<br><br><br><br><br><br><br>
	<h1 class="ui teal centered dividng header"><i class="big teal frown icon"></i>Lo sentimos,</h1><br>
	<p style="text-align: center">No hemos podido encontrar su correo electrónico. Por favor comuníquese a <a href="mailto:info@ccjpv.com">info@ccjpv.com</a></p>
</div>
