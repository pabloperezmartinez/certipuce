<div class="ui container">
	<div class="ui segment">
		<h3 class="ui teal dividing header">Para uso de CCJPV</h3>
		<?php if ($datosFacturacion['rutaEvidenciaPago']!='' && $datosFacturacion['rutaEvidenciaPago']!=null):?>
		<div class="ui teal sub header">Comprobante de pago</div><br>
		<img src="<?php echo base_url('/application/cargasDocumentos/comprobantes/'.$datosFacturacion['rutaEvidenciaPago'])?>" class="ui big centered image"/><br>
		<?php endif;?>
		<?php if ($participante['documentoDescuento'] != null):?>
			<div class="ui teal sub header">Tipo de descuento solicitado: <?php echo $descuentoRegistrado['descripcionDescuento']?></div>
			<img src="<?php echo base_url('/application/cargasDocumentos/comprobantes/descuentos/'.$participante['documentoDescuento'])?>" class="ui big centered image"/><br>
		<?php endif;?>
		<pre><?php // print_r($datosFacturacion)?></pre>
		<div action="<?php echo base_url('/admin/participante/actualizarDatosFinancieros/'.$datosFacturacion['idParticipanteEvento'])?>" method="post" class="ui form">
			<div class="ui two column stackable grid">
				<div class="column">
					<div class="field">
						<a class="ui right floated teal mini button" onclick="$('#editarNumeroFactura').modal('show')"><i class="edit icon"></i></a>
						<label>No. de Factura</label>
						<div><?php echo $datosFacturacion['facturaCiespalParticipanteEvento']?></div>
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Valor Pagado</label>
						<div>$ <?php echo number_format($datosFacturacion['valorPagadoParticipanteEvento'], 2)?></div>
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Inscripción Completa</label>
						<div>Sí</div>
					</div>
					<div class="field">
						<div class="ui checkbox">
							<input name="certificado" type="checkbox" tabindex="0" disabled="disabled">
							<label>Certificado Entregado</label>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Observaciones</label>
						<div><?php echo $datosFacturacion['observacionesParticipanteEvento']?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ui small modal" id="editarNumeroFactura">
	<div class="header">Editar número de factura</div>
	<i class="close icon"></i>
	<div class="content">
		<form class="ui form">
			<div class="field">
				<label>Número de Factura</label>
				<input id="numeroFactura" type="text">
			</div>
			<div class="ui right floated teal button" onclick="actualizarFactura()">Guardar</div>
		</form><br>
	</div>
</div>


<script>
function actualizarFactura(){
	$.ajax({
        type:"POST",
        dataType:"html",
        url: baseUrl+"admin/participante/actualizarFacturacion/" + <?php echo $datosFacturacion['idParticipanteEvento']?>,
        data: { numeroFactura: ""+$('#numeroFactura').val()},
        success:function(msg){
        	location.reload();
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });
}
</script>
