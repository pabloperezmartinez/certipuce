<div class="ui container"><br>
	<a href="<?php echo base_url('/admin/evento/detalles/'.$idEvento)?>">
	<span class="ui icon button iconmargen">
  		<i class="reply icon"></i>
	</span>
	</a><br><br>
	<div class="ui two column grid">
		<div class="column">
			<div class="ui segment">
				<h2 class="ui teal dividing header">Detalles de participante</h2>
				<p><strong>Apellidos: </strong><?php echo $participante['apellidosParticipante']?></p>
				<p><strong>Nombres: </strong><?php echo $participante['nombresParticipante']?></p>
				<p><strong>Cédula de Identidad: </strong><?php echo $participante['identificacionParticipante']?></p>
				<p><strong>Ciudad: </strong><?php echo $participante['ciudadParticipante']?></p>
				<p><strong>País:</strong>&nbsp;&nbsp;&nbsp; <i class="<?php echo $pais['iso_a2']?> flag"></i><?php echo $pais['descripcionPais']?> </p>
				<p><strong>Institución: </strong><?php echo $participante['institucionParticipante']?></p>
				<p><strong>Teléfono: </strong><?php echo $participante['telefonoParticipante']?></p>
				<?php if ($participante['telefono2Participante']!=null || $participante['telefono2Participante']!=''):?>
					<p><strong>Teléfono alterno: </strong><?php echo $participante['telefono2Participante']?></p>
				<?php endif;?>
				<p><strong>Email: </strong><?php echo $participante['correoElectronicoParticipante']?></p>
			</div>
		</div>
		<div class="column">
			<div class="ui segment">
				<h2 class="ui teal dividing header">Datos de facturación</h2>
				<!-- <pre><?php print_r($datosFacturacion); ?></pre> -->
				<p><strong>RUC: </strong><?php echo $datosFacturacion['rucDatosFacturacion']?></p>
				<p><strong>Razón Social: </strong><?php echo $datosFacturacion['razonSocialDatosFacturacion']?></p>
				<p><strong>Dirección: </strong><?php echo $datosFacturacion['direccionDatosFacturacion']?></p>
				<p><strong>Teléfono: </strong><?php echo $datosFacturacion['telefonoDatosFacturacion']?></p>
			</div>
			<div class="ui segment">
				<h2 class="ui teal dividing header">Datos de matrícula</h2>
				<p><strong>Certificado: </strong><?php echo $datosFacturacion['certificadoParticipanteEvento']?></p>
				<p><strong>Tipo de Participante: </strong><?php echo $tipoParticipante['descripcionTipoParticipante']?></p>
				<p><strong>Valor a pagar: </strong>$<?php echo number_format($datosFacturacion['valorAPagar'], 2)?></p>
				<p><strong>Forma de pago: </strong><?php echo $formaPago['descripcionTipoFormaPago']?></p>
			</div>
		</div>
	</div>
</div><br>
