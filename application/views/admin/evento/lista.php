<script type="text/javascript" src="<?php echo base_url('application/assets/js/listaEvento.js') ?>"></script>
<div class="ui container" style="min-height:500px">
	<span style="float: right">
		<?php if ($this->session->userdata('idTipoEmpleado')==1 || $this->session->userdata('idTipoEmpleado')==2):?>
		<a href="<?php echo base_url('/admin/evento/crear');?>">
			<i class="big green add icon"></i>
		</a>
		<?php endif;?>
	</span>
	<h1 class="ui teal dividing header">Eventos</h1>

	<table id="eventos" class="ui selectable definition table">
		<thead>
			  <tr>
			  	<th></th>
			    <th>Evento</th>
			    <th>Fecha de inicio</th>
			    <th>Fecha de fin</th>
			    <th></th>
			  </tr>
		</thead>
		<tbody>
		  	<?php
			  $i=0;
			  foreach ($eventos as $evento):?>
			  <tr>
			  	  <td><?php $i++; echo $i;?></td>
			      <td><?php echo $evento['tituloEvento']?></td>
						<?php
						 	$fechaInicioEvento = new DateTime($evento['fechaInicioEvento']);
							$fechaInicioEvento = new Carbon\Carbon($fechaInicioEvento->format(DATE_ISO8601));
 						 	$fechaFinEvento = new DateTime($evento['fechaFinEvento']);
 							$fechaFinEvento = new Carbon\Carbon($fechaFinEvento->format(DATE_ISO8601));
 						 ?>
			      <td><?php echo $fechaInicioEvento->formatLocalized('%A, %d de %B de %Y');?></td>
			      <td><?php echo $fechaFinEvento->formatLocalized('%A, %d de %B de %Y');?></td>
			      <td>
		      		<a title="Detalles de evento" href="<?php echo base_url('/admin/evento/detalles/'.$evento['idEvento']);?>"><i class="big teal browser icon"></i></a>
			      	<?php if ($this->session->userdata('idTipoEmpleado')==1 || $this->session->userdata('idTipoEmpleado')==2):?>
			      	<a href="<?php echo base_url('/admin/evento/modificarEvento/'.$evento['idEvento']);?>">
				  		<i class="green big edit icon"></i>
					</a>
			      	<a title="Eliminar Evento" href="#" onclick="desplegarConfirmacionEliminacion(<?php echo $evento['idEvento']?>,'<?php echo $evento['tituloEvento']?>')"><i class="big red delete icon"></i></a>
			      	<?php endif;?>
			      </td>
			  </tr>
			  <?php endforeach;?>
		</tbody>
	</table>
</div>
