<html class="ios">
	<head>
		<meta charset="UTF-8">
		<link rel="icon" href="<?php echo base_url("application/assets/img/favicon.ico.png")?>" type="image/png">
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery-2.1.3.min.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/semantic.min.css') ?>">
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script src="<?php echo base_url('application/assets/semantic-ui/semantic.min.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/functions.js') ?>"></script>
		<link rel="stylesheet/less" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/src/definitions/collections/menu.less') ?>">
		<link rel="stylesheet" href="<?php echo base_url('application/assets/custom.css') ?>" type="text/css">
		<title> Sistema de Matrículas</title>
	</head>
	<body style="height: 100%; !important">
		<div class="ui text container margen">
			<img class="ui centered medium image" src="<?php echo base_url('application/assets/images/ccjpv-teal.png')?>"/>
			<div class="ui center aligned grid">
				<div class="column">
					<h3 class="ui image header margenmod">
					<div class="ui teal header content">
						Sistema de Matrículas
					</div>
				</h3>
				<form id="loginForm" action="<?php echo base_url('admin')?>" method="post" class="ui large form">
					<div class="ui stacked segment">
						<div class="field">
							<div class="ui left icon input">
								<i class="mail icon"></i>
								<input type="text" id="email" name="email" placeholder="e-mail">
							</div>
						</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								<input type="password" id="contrasena" name="contrasena" placeholder="Contraseña">
							</div>
						</div>
						<input type="submit" value="Ingresar" class="ui basic primary button">
					</div>
				</form>
					<?php if (isset($error)){?>
					<div class="ui error message"><?php echo $error;?></div>
					<?php }?>
				</div>
			</div>
		</div>
		<div style="text-align: center; font-size: 10px; position:fixed; bottom: 5px; right: 5px">
			<b>Navegador Recomendado</b><br>
			<img class="ui mini centered image" src="<?php echo base_url('application/assets/images/chrome.gif')?>">
			Google Chrome 50 o superior
		</div>
	</body>
</html>
