<br>
<div class="ui container segment">
	<h1 class="ui teal dividing header"><?php echo $evento['tituloEvento']?></h1>
	<div class="ui two column grid">
		<div class="column">
			<h3 class="ui teal header">Fechas</h3>
			<p><strong>Fecha y Hora de Inicio: </strong><?php echo $evento['fechaInicioEvento']?><br></p>
			<p><strong>Fecha y Hora de Finalización: </strong><?php echo $evento['fechaFinEvento']?></p><br><br>
			<p><strong>Emisión certificado: </strong><?php if ($evento['certificadoEvento']==0) echo 'No'; else echo 'Sí';?></p>
		</div>
		<div class="column">
			<div class="ui info message">
				<div class="header">Costos</div><br>
				<?php foreach ($costos as $costo):?>
					<b><?php echo ucfirst(strtolower($costo['descripcionTipoParticipante']))?>: </b>$ <?php echo number_format($costo['costoValorEvento'],2)?> USD<br>
				<?php endforeach;?>
			</div>
			<div class="ui brown message">

				<div>Por favor ingrese sus datos verdaderos, de tal forma que podamos contactarlo.</div>
			</div>
		</div>
	</div>

	<form action="" class="ui form" method="post" enctype="multipart/form-data">
		<div class="ui stackable grid">
			<div class="one column row"><br>
				<div class="column"><h3 class="ui teal dividing header">Datos de participante</h3></div>
			</div>
			<div class="two column row">
				<div class="column"><br>
					<div class="required field" id="apellidosParticipante">
						<label>Apellidos: </label>
						<input type="text" name="apellidosParticipante" onblur="autoCompletarCertificado()">
					</div>
				</div>
				<div class="column"><br>
					<div class="required field" id="nombresParticipante">
						<label>Nombres: </label>
						<input type="text" name="nombresParticipante" onblur="autoCompletarCertificado()">
					</div>
				</div>
				<div class="column"><br>
					<div class="required field">
						<label>Tipo de identificación: </label>
						<select id="idTipoIdentificacion" name="idTipoIdentificacion" class="ui search selection dropdown">
							<?php foreach ($tiposIdentificacion as $tipoIdentificacion):?>
								<option value="<?php echo $tipoIdentificacion['idTipoIdentificacion']?>"><?php echo $tipoIdentificacion['descripcionTipoIdentificacion']?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="column"><br>
					<div class="required field" id="identificacionParticipante">
						<label>Número de identificación: </label>
						<input type="text" name="identificacionParticipante">
					</div>
				</div>
				<div class="column"><br>
					<div class="required field">
						<label>País: </label>
						<div id="idPais" class="ui fluid search selection dropdown">
						  <input type="hidden" name="idPais" value="63">
						  <i class="dropdown icon"></i>
						  <div class="default text">Select Country</div>
						  <div class="menu">
								<?php foreach ($paises as $pais):?>
									<div class="item" data-value="<?php echo $pais['idPais']?>"><i class="<?php echo strtolower($pais['iso_a2']);?> flag"></i><?php echo $pais['descripcionPais']?></div>
								<?php endforeach;?>
							</div>
						</div>
					</div>
				</div>
				<div class="column"><br>
					<div class="field" id="ciudadParticipante">
						<label>Ciudad: </label>
						<input type="text" name="ciudadParticipante">
					</div>
				</div>
				<div class="column"><br>
					<div class="field" id="institucionParticipante">
						<label>Institución: </label>
						<input type="text" name="institucionParticipante">
					</div>
				</div>
				<div class="column"><br>
					<div class="required field" id="correoElectronicoParticipante">
						<label><i class="mail icon"></i>Email: </label>
						<input type="text" name="correoElectronicoParticipante">
					</div>
				</div>
				<div class="column"><br>
					<div class="two fields">
						<div class="required field" id="telefonoParticipante">
							<label><i class="large phone icon"></i>Teléfono: </label>
							<input type="text" name="telefonoParticipante" maxlength="15">
						</div>
						<div class="field" id="estensionTelefonoParticipante" >
							<label>Extensión: </label>
							<input type="text" name="extensionTelefonoParticipante" maxlength="5">
						</div>
					</div>
				</div>
				<div class="column"><br>
					<div class="two fields">
						<div class="field" id="telefono2Participante">
							<label><i class="phone large icon"></i>Teléfono alterno o <i class="mobile large icon"></i>móvil: </label>
							<input type="text" name="telefono2Participante" maxlength="15">
						</div>
						<div class="field" id="estensionTelefono2Participante">
							<label>Extensión de teléfono alterno: </label>
							<input type="text" name="extensionTelefono2Participante" maxlength="5">
						</div>
					</div>
				</div>
				<div class="column"><br>
					<div class="required field">
						<label>Contraseña: </label>
						<input type="password" name="contrasenaParticipante">
					</div>
				</div>
				<div class="column"><br>
					<div class="required field">
						<label>Verificar Contraseña: </label>
						<input type="password" name="verificarContrasena">
					</div>
				</div>
			</div>

			<?php if(!$this->evento_model->esGratuito($evento['idEvento'])):?>
			<div class="one column row" id="datosFact">
				<div class="column">
				<br>
					<h3 class="ui dividing teal header">Datos de facturación</h3>
					<input type="hidden" name="idDatosFacturacion" value="Nueva"/>
					<span style="float: right;"><a href='#datosFact' onclick="copiarValores()" class="ui teal sub header"><i class="ui teal large copy icon"></i>Copiar desde datos personales</a></span>
					<div class="required field" id="rucDatosFacturacion">
						<label>RUC: </label>
						<input type="text" name="rucDatosFacturacion">
					</div>
					<div class="required field" id="razonSocialDatosFacturacion">
						<label>Razón Social: </label>
						<input type="text" name="razonSocialDatosFacturacion">
					</div>
					<div class="required field" id="direccionDatosFacturacion">
						<label>Dirección: </label>
						<input type="text" name="direccionDatosFacturacion">
					</div>
					<div class="required field" id="telefonoDatosFacturacion">
						<label>Teléfono: </label>
						<input type="text" name="telefonoDatosFacturacion">
					</div>
				</div>
			</div>
			<?php endif;?>

			<div class="one column row">
				<div class ="column">
					<h3 class="ui teal dividing header">Descuentos</h3>
				</div>
			</div>
			<div class="three column row">
				<div class="column" id="textoDescuento">
					<div class="ui info message">
						<div class="header">Importante!</div>
						Si usted desea adquirir cualquiera de nuestros descuentos deberá cargar a nuestro sistema el comprobante correspondiente, mediante un escaneado o una foto del mismo
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Descuento</label>
						<div id="descuento" class="ui selection dropdown">
							<input type="hidden" name="descuento">
						  	<i class="dropdown icon"></i>
							<div class="default text">Elija un tipo de Descuento</div>
							<div class="menu">
								<div class="item" data-value="0">Ninguno</div>
								<?php foreach ($descuentos as $descuento):?>
							    <div class="item" data-value="<?php echo $descuento['idDescuento']?>">
							    	<?php if ($descuento['descripcionDescuento']=="Estudiante"):?>
							    		<i class="large student icon"></i>
							    	<?php elseif ($descuento['descripcionDescuento']=="Discapacidad"):?>
							    		<i class="large handicap icon"></i>
							    	<?php elseif ($descuento['descripcionDescuento']=="Adulto Mayor"):?>
							    		<i class="large spy icon"></i>
							    	<?php endif;?>
							    	<?php echo $descuento['descripcionDescuento']?> - <?php echo $descuento['tazaDescuento'] * 100?>%
							    </div>
							    <?php endforeach;?>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="field" id="validacionDescuento" style="display: none;">
						<label id="labelComprobanteDescuento">Documento de validación</label>
						<input type="file" name="comprobanteDescuento" id="inputComprobanteDescuento" disabled="disabled">
					</div>
				</div>
			</div>

			<div class="one column row">
				<div class="column"><h3 class="ui dividing teal header">Datos de Matrícula</h3></div>
			</div>
			<div class="two column row">
				<div class="column">
					<div class="required field">
	      				<label>Usted participa como:</label>
						<select id="idTipoParticipante" name="idTipoParticipante" class="ui search selection dropdown">
							<?php foreach ($costos as $costo):?>
								<option value="<?php echo $costo['idTipoParticipante']?>"><?php echo $costo['descripcionTipoParticipante']?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="column">
					<div class="required field">
						<label>Valor a pagar:</label>
						<input type="text" id="despliegueCostos" name="valorAPagar" value="<?php echo number_format($costos[0]['costoValorEvento'], 2)?>" readonly>
					</div>
				</div>
				<?php if(!$this->evento_model->esGratuito($evento['idEvento'])):?>
				<div class="column">
					<div class="required field">
						<label>Forma de pago:</label>
						<select id="formaPago" name="idTipoFormaPago"  class="ui search selection dropdown">
							<?php foreach ($formasPago as $formaPago):?>
								<option value="<?php echo $formaPago['idTipoFormaPago']?>"><?php echo $formaPago['descripcionTipoFormaPago']?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="column">
					<div class="field" id="comprobante" style="display:none">
						<label>Comprobante de depósito:</label>
						<input type="file" id="comprobantePago" name="comprobantePago">
					</div>
					<div id="mensajePagoDeposito" class="ui info message" style="display:none">
					  	<i class="close icon" onclick="$('.ui.info.message').transition('slide')"></i>
					  	<div class="header">
					    	Pago mediante transferencia:
					  	</div>
							<div class="content">
								Se debe depositar a la cuenta de <br /><strong>Ahorros de Banco Pichincha # 3291569200</strong><br />a nombre de
								<strong>Inés Martínez Moreno</strong>,<br />CI/RUC: <strong>0601273287</strong>.<br><br>
								Una vez realizado el depósito deberá agregar el depósito en el campo correspondiente
							</div>
					 </div>
					<div id="mensajePagoEfectivo" class="ui info message" style="display:none">
					  	<i class="close icon" onclick="$('.ui.info.message').transition('slide')"></i>
					  	<div class="header">
					    	Pago en efectivo:
					  	</div>
							<div class="content">
								Un representante del Centro de Capacitación José Pedro Varela se comunicará con usted para
								confirmar su asistencia al evento, usted podrá pagar hasta el día del inicion del mismo.
							</div>
					 </div>
				</div>
				<?php endif;?>
			</div>
			<div class="one column row">
				<?php if(!$this->evento_model->esGratuito($evento['idEvento'])):?>
				<div class="column">
					<div class="field">
						<label>Detalles de comprobante de pago:</label>
						<textarea id="descripcionEvidenciaPago" name="descripcionEvidenciaPago"></textarea>
					</div>
				</div>
				<?php endif;?>
				<?php if ($evento['certificadoEvento']!=0):?>
				<div class="column">
					<div class="required field" id="certificado">
						<label>Nombre del Certificado:</label>
						<input type="text" id="certificadoParticipanteEvento" name="nombreCertificado" value="">
					</div>
				</div>
				<?php endif;?>
				<div class="center aligned column"><br>
					<input type="submit" value="Guardar Datos" class="ui teal button">
					<div class="ui cancel button">Limpiar Formulario</div>
				</div>
			</div>

		</div>

	</form>
</div>
<script type="text/javascript">

function autoCompletarCertificado(){
	$('#certificadoParticipanteEvento').val($('#nombresParticipante>input').val()+' '+$('#apellidosParticipante>input').val().toUpperCase());
}

var costos = <?php echo json_encode($costos); ?>;
var descuentos = <?php echo json_encode($descuentos); ?>;

$('.ui.checkbox').checkbox({
	onChecked: function() {
		$('h5').removeClass('inverted');
		//$('.ui.segment').removeClass('teal inverted');
		$('h5').addClass('teal');
		$('#facturas .ui.segment').removeClass('teal inverted');
		$('#factura'+this.value).addClass('teal inverted');
		$('#factura'+this.value +' h5 ').removeClass('teal');
		$('#factura'+this.value +' h5 ').addClass('inverted');
	},
});

$('#idTipoParticipante')
.dropdown({
	onChange: function(value, text, selectedItem) {
	      $.each(costos, function(i, costo){
		      var valorInicial=0;
		      $.each(costos, function(i, costo){
					if(costo.idTipoParticipante==value){
							valorInicial=costo.costoValorEvento;
						}

		      });
		      idDescuento=$('#descuento>input').val();
		      var tazaDescuento=0;
		      if ($('#descuento>input').val()!=0 && $('#descuento>input').val()!=null){
		      $.each(descuentos, function(i, descuento){
					if(descuento.idDescuento==$('#descuento>input').val()){
							tazaDescuento=descuento.tazaDescuento;
						}
			      })
		      };
		      valorCalculado=valorInicial*(1-tazaDescuento)
		      if (costo.idTipoParticipante==value)
		    	  $('#despliegueCostos').val(''+valorCalculado.toFixed(2));
	    });
	}
});

$('#descuento')
.dropdown({
	onChange: function (value, text, selectedItem){
		idCosto=$('#idTipoParticipante>input').val();
		  var valorInicial=0;
	      var tazaDescuento=0;
	// labelComprobanteDescuento
		  var descripcionDescuento='';
		  $.each(costos, function(i, costo){
					if(costo.idTipoParticipante==$('#idTipoParticipante').val()){
							valorInicial=costo.costoValorEvento;
						}

		  });
		var nombre
		  if (value!=0 && value!=null){
			 	$('#comprobante').css('display', 'block');
			 	$('#comprobante>input').removeAttr('disabled');
		      	$.each(descuentos, function(i, descuento){
					if(descuento.idDescuento==value){
						tazaDescuento=descuento.tazaDescuento;
						descripcionDescuento=descuento.descripcionDescuento;
					}
		  		})
		  }else{
		    	  $('#comprobante').css('display', 'none');
				  $('#comprobante>input').attr('disabled','disabled');
				  tazaDescuento=0;
		  }
		  if (descripcionDescuento=="Estudiante")
			  $('#labelComprobanteDescuento').text('Carnet Estudiantil')
		  if (descripcionDescuento=="Discapacidad")
			  $('#labelComprobanteDescuento').text('Carnet de CONADIS')
		  if (descripcionDescuento=="Adulto Mayor")
			  $('#labelComprobanteDescuento').text('Cédula de Identidad')
		  var valorCalculado=valorInicial*(1-tazaDescuento);
		  $('#despliegueCostos').val(valorCalculado.toFixed(2));
	  }
})

$('#formaPago').dropdown({
  onChange: function(value, text, $selectedItem) {
		console.log('entra');
		if (value == 2) {
				$('#mensajePagoEfectivo').hide();
				$('#comprobante').show();
				$('#mensajePagoDeposito').show();
				$("#comprobantePago").prop('disabled', false);
		} else if (value == 3) {
				$('#comprobante').hide();
				$('#mensajePagoDeposito').hide();
				$('#mensajePagoEfectivo').show();
				$("#comprobantePago").prop('disabled', true);
		}
  }
});

$('#idPais')
.dropdown()
;
$('#idTipoIdentificacion')
.dropdown()
;

var formulario

$( document ).ready(function() {


$.fn.form.settings.rules.validacionComprobantePago= function(value) {
	extension=value.substr(value.length - 4);
	if(value=='' || extension==".gif" || extension==".jpg" || extension=="jpeg" || extension==".png")
		return true;
	return false;
}

$.fn.form.settings.rules.emailExiste= function(value) {
	var email=encodeURIComponent(value);
	var resultado='';
	$.ajax({
            type:"GET",
            async: false,
            url: baseUrl+"participante/verificarEmail/"+email,
            success : function(text){
            	resultado = text;
            }
        });
    if(resultado=="0")
        return true
    return false
}

formulario=$('.ui.form')
 .form({
	  fields: {
			apellidosParticipante: {
		      identifier: 'apellidosParticipante',
		      rules: [
		        {
		          type   : 'empty',
		          prompt : 'Por favor, debe ingresar su(s) apellido(s)'
		        }
		      ]
		    },
		    nombresParticipante: {
			      identifier: 'nombresParticipante',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor, debe ingresar su(s) Nombre(s)'
			        }
			      ]
			    },
		    identificacionParticipante: {
			      identifier: 'identificacionParticipante',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor, ingrese su número de identificación'
				    }
			      ]
			    },
		    correoElectronicoParticipante: {
			      identifier: 'correoElectronicoParticipante',
			      rules: [
					{
					  type   : 'emailExiste',
					  prompt : 'El correo electrónico ya está registrado en nuestro sistema.'
					},
					{
			          type   : 'email',
			          prompt : 'Por favor, ingrese un email válido'
			        }
			      ]
			    },
		    contrasenaParticipante: {
			      identifier: 'contrasenaParticipante',
			      rules: [
			          {
			            type   : 'empty',
			            prompt : 'Ingrese una contraseña'
			          },
			          {
			            type   : 'minLength[6]',
			            prompt : 'Su contraseña debe tener almenos {ruleValue} caracteres'
			          }
			      ]
			    },
		    verificarContrasena: {
			      identifier: 'verificarContrasena',
			      rules: [
			          {
			            type   : 'match[contrasenaParticipante]',
			            prompt : 'Las contraseñas no coinciden'
			          }
			      ]
			    },
			ruc: {
			      identifier: 'rucDatosFacturacion',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor debe ingresar un ruc'
			        }
			      ]
			  },
			razonSocial: {
			      identifier: 'razonSocialDatosFacturacion',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor debe ingresar una Razón Social'
			        }
			      ]
			  },

			direccion: {
			      identifier: 'direccionDatosFacturacion',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor debe ingresar una Dirección Válida'
			        }
			      ]
			  },
			telefono: {
			      identifier: 'telefonoDatosFacturacion',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor debe ingresar un valor numérico para almacenar su teléfono'
			        }
			      ]
			  },
			ciudadParticipante: {
			      identifier: 'ciudadParticipante',
			      rules: [
			        {
			          type   : 'empty',
			          prompt : 'Por favor debe ingresarla su ciudad de residencia'
			        }
			      ]
			  },
			telefonoParticipante: {
			      identifier: 'telefonoParticipante',
			      rules: [
			        {
			          type   : 'regExp[^[0-9]{9,15}]',
			          prompt : 'Por favor ingresar número telefónico con código de país y estado/provincia (solo números)'
			        }
			      ]
			  },
			comprobante: {
			    identifier: 'comprobantePago',
			    rules: [
							{
								type   : 'empty',
	             	prompt : 'Debe agregar comprobante de depósito'
			        },
			        {
			          type   : 'validacionComprobantePago',
			          prompt : 'Solo puede ingresar el comprobante en formato de imagen GIF, JPG o PNG'
			        }
			      ]
		  	},
		  	comprobanteDescuento: {
		  		identifier: 'comprobanteDescuento',
			      rules: [
			        {
			          type   : 'validacionComprobantePago',
			          prompt : 'Solo puede ingresar el comprobante en formato de imagen GIF, JPG o PNG'
			        },
			        {
				      type   : 'empty',
				      prompt : 'Por favor debe debe ingresar el escaneado o foto del documento'
				    }
			      ]
			},
	  },
	  inline: true,
	  on: 'blur'
	})
	;
});

function copiarValores(){
	$('#telefonoDatosFacturacion>input').val($('#telefonoParticipante>input').val());
	$('#rucDatosFacturacion>input').val($('#identificacionParticipante>input').val());
	$('#razonSocialDatosFacturacion>input').val($('#nombresParticipante>input').val()+' '+$('#apellidosParticipante>input').val());
}

function comprobarFormaPago() {
	if ($('#formaPago').dropdown('get value') == '2') {
			//pago por depósito
			$('#comprobante').css('display', 'block');
			$('#mensajePagoDeposito').show();
			$("#comprobantePago").prop('disabled', false);
	} else if ($('#formaPago').dropdown('get value') == '3') {
			//Pago en efectivo
			$('#mensajePagoEfectivo').show();
			$("#comprobantePago").prop('disabled', true);
	}
}

comprobarFormaPago();
</script>
