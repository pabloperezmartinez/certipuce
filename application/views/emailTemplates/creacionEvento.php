<html>
	<body>
		<div class="ui text container">
			<div class="ui segment">
				<h3 class="ui blue header">Estimad@ <?php echo $empleado['nombresEmpleado']?> <?php echo $empleado['apellidosEmpleado']?> : </h3>
				<p>Le informamos que el evento "<strong><?php echo $evento['tituloEvento']?></strong>" ha sido creado:</p>
				<p>Identificador de evento: <strong><?php echo $evento['idEvento']?></strong></p>
				<p>Fecha de inicio: <strong><?php echo $evento['fechaInicioEvento']?></strong></p>
			</div>
			<div style="text-align: right; font-size: 9pt">
				<img width="150px" src="<?php echo base_url('application/assets/images/ccjpv-teal.png')?>"/><br>
				Tomás de Berlanga E10-115 e Isla Pinzón (esquina)<br>
				Quito, Ecuador.<br>
				Telfs: (593 2) 2453-585<br>
				<a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
			</div>
		</div>
	</body>
</html>
