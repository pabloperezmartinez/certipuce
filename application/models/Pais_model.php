<?php
/**
 * Clase de Gestión de países
 * @author TICs CIESPAL
 *
 */

class Pais_model extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}
	
	/**
	 * obtiene un país específico o el listado completo de la tabla en orden alfabético
	 * @param int $id
	 */
	public function obtenerPaises($id=FALSE) {
		if ($id===FALSE){
			$this->db->select('*');
			$this->db->from('pais');
			$this->db->where('estadoPais', 1);
			$this->db->order_by('descripcionPais', 'ASC');
			
			$query=$this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('pais');
			$this->db->where('idPais', $id);
			$query=$this->db->get();
			return $query->row_array();
		}
	}
}