<?php
class Tipoevento_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}
	
	/**
	 * Obtiene un arreglo de todos los tipos de evento
	 */
	public function obtenerTiposEvento(){
		$this->db->select('*');
		$this->db->from('tipoevento');
		$this->db->where('estadoTipoEvento', 1);
		$this->db->order_by('descripcionTipoEvento', 'ASC');
		$query=$this->db->get();
		return $query->result_array();
	}
}
?>