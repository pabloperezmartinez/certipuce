<?php
class Tipoparticipante_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}
	/**
	 * Obtiene todos los tipos de participante
	 */
	public function obtenerTiposParticipante(){
		$this->db->select('*');
		$this->db->from('tipoparticipante');
		$this->db->where('estadoTipoParticipante', 1);
		$this->db->order_by('descripcionTipoParticipante', 'ASC');
		$query=$this->db->get();
		return $query->result_array();
	}
	
	/**
	 * Devuelve la descripción de un tipo de PArticipante
	 * @param int $idTipoParticipante
	 */
	public function obtenerDescripcionTipoParticipante($idTipoParticipante){
		$this->db->select('descripcionTipoParticipante');
		$this->db->from('tipoparticipante');
		$this->db->where('idTipoParticipante', $idTipoParticipante);
		$query=$this->db->get();
		return $query->row_array();
	}

}
?>