<?php

/**
* Clase para gestión de descuentos
* @author Pablo Pérez
*
*/
class Descuento_model extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}

	/**
	 * Devuelve un arreglo de los descuentos NO GRUPALES
	 * @return array
	 */
	public function obtenerNoGrupales(){
		$this->db->select('*');
		$this->db->from('descuento');
		$this->db->where('idDescuento!=', 4);
		
		$query=$this->db->get();
		return $query->result_array();
	}
	
	/**
	 * obtiene un descuento específico
	 * @param int $idDescuento
	 * @return array
	 */
	public function obtenerDescuento($idDescuento){
		$this->db->select('*');
		$this->db->from('descuento');
		$this->db->where('idDescuento', $idDescuento);
	
		$query=$this->db->get();
		return $query->row_array();
	}

}