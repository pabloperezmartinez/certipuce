<?php

/**
 * Clase para gestión de auditoría
 * @author Pablo Pérez
 *
 */
class Auditoria_model extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}

	public function ingresarLog($accion){
		$data = array(
				'fecha' => date('Y-m-d H:i:s',now()),
				'idEmpleado' => $this->session->userdata('idEmpleado'),
				'accion' => $accion
		);

		$this->db->insert('auditoria', $data);

		//return $this->db->insert_id();
	}

}
