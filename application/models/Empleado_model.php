<?php

/**
 * Modelo de Empleado
 * @author tics
 *
 */

class Empleado_model extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->helper('date');
	}

	/**
	 * Realiza la comprobación del email del empleado y la contraseña
	 * @param string $email
	 * @param string $contrasena
	 */
	public function loginEmpleado($email, $contrasena){
		$this->db->select('*');
		$this->db->from("empleado");
		$this->db->where("correoElectronicoEmpleado", $email);
		$this->db->where("contrasenaEmpleado", $contrasena);
		$this->db->where("estadoEmpleado", 1);

		$query=$this->db->get();

		if ( $query->num_rows() == 0)
			return null;

		return $query->row_array();
	}

	/**
	 * Devuelve listado de empleados o un empleado específico
	 * @param int $idEmpleado
	 */
	public function obtenerEmpleado($idEmpleado=FALSE){
		if ($idEmpleado === FALSE){
			$this->db->select('*');
			$this->db->from('empleado');
			$this->db->where('estadoEmpleado', 1);
			$this->db->where('estadoEmpleado', 1);
			$query=$this->db->get();
			return $query->result_array();
		}
		$query = $this->db->get_where('empleado', array(
					'idEmpleado' => $idEmpleado,
					'estadoEmpleado' => 1
		));
		return $query->row_array();
	}

	/**
	 * Obtiene la lista de empleado que no son administradores
	 * @return array
	 */
	public function obtenerEmpleadoNoAdministrador(){
		$this->db->select('*');
		$this->db->from('empleado');
		$this->db->where('estadoEmpleado', 1);
		$this->db->where('idTipoEmpleado!=', 2);
		$query=$this->db->get();
		return $query->result_array();
	}

	/**
	 * Crea empleado en la base de datos
	 * @param string $email
	 * @param string $contrasena
	 * @param string $apellidosEmpleado
	 * @param string $nombresEmpleado
	 */
	public function crearEmpleado($email, $contrasena, $apellidosEmpleado, $nombresEmpleado){
		$empleadoData = array(
				'correoElectronicoEmpleado' => $email,
				'contrasenaEmpleado' => $contrasena,
				'apellidosEmpleado' => $apellidosEmpleado,
				'nombresEmpleado' => $nombresEmpleado,
				'estadoEmpleado' => 1,
				'idTipoEmpleado' => 4
		);
		$this->db->insert('empleado', $empleadoData);

		return $this->db->insert_id();
	}
}
