var baseUrl="http://certipuce.test/";
var assetsUrl= baseUrl + "application/assets/";
var uploadsUrl= baseUrl + "application/cargasDocumentos/";


$( document ).ready(function() {
 formulario=$('#loginForm')
	.form({
	  fields: {
		  email: {
	      identifier: 'email',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor debe ingresar un email'
	        },
          {
            type   : 'email',
            prompt : 'Por favor ingrese un email correcto'
          }
	      ]
	    },
      contrasena: {
	      identifier: 'contrasena',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor debe ingresar una contraseña'
	        }
	      ]
	    }
	  },
	  inline: true
	})
	;
});
