function mostrarCertificado(idParticipanteEvento){

	$('#certificadoModal').remove();

	$('body').append('<div id="generancionCertificado" class="ui active dimmer"><div class="ui large text loader"></div></div>')
	$.ajax({
        type:"GET",
        dataType:"html",
        url: baseUrl+"admin/participante/generarCertificado/" + idParticipanteEvento,
        success:function(msg){
					console.log(msg);
        	console.log(JSON.parse(msg));
        	url=JSON.parse(msg);
        	$('#generancionCertificado').remove();
        	$('body').append(''+
    			'<div class="ui modal" id="certificadoModal" style="width:80%" >'+
    			 '<i class="close icon"></i>'+
    			  '<div class="header">'+
    			  	'<span style="float:right"> '+
    			  		'<a title="Imprimir" href="#" onclick="imprimirCertificado(\'' + uploadsUrl + 'certificados/PDF/' + url + '\', '+ idParticipanteEvento +')"><i class="ui blue print icon"></i><a>'+
    			  		'<a href="#" onclick="enviarCertificado(' + idParticipanteEvento + ')"><i class="ui blue send icon"></i></a>'+
    			  	'</span>'+
    			    'Certificado'+
    			  '</div>'+
    			  '<div id="despliegue" class="content">'+
							'<iframe width = "100%" height = "500px"' +
							  'src=\"http://docs.google.com/gview?url='+ uploadsUrl + 'certificados/DOCX/' + url  + '&embedded=true\"' +
							  'frameborder=\"0\">'+
							'</iframe>' +
						//'<iframe src="' + assetsUrl +'/js/pdfjs/web/viewer.html?file='+ uploadsUrl + 'certificados/PDF/' + url + '" width="100%" height="600" ></iframe>' +
    			  '</div>'+
    			  '<div id="mensajeEnvioCorreo" class="ui yellow inverted center aligned segment" style="position: fixed; top: 45%; width: 100%; display: none; z-index: 10 !important;"><i class="mail icon"></i> Certificado enviado correctamente</div>'+
    			'</div>'
        	);
        	$('#certificadoModal').modal('show');
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });

	$.ajax({
        type:"GET",
        dataType:"html",
        url: baseUrl+"admin/participante/revisarEntregaCertificado/" + idParticipanteEvento,
        success:function(msg){
        	console.log(JSON.parse(msg));
        	if(JSON.parse(msg)==true){
        		$('#despliegue').prepend(''+
        			'<div id="impresoEntregado" class="ui warning message">'+
    				  '<div class="header">' +
    				   	'Certificado Entregado' +
    				  '</div>'+
    				  'El certificado impreso ya fue entregado previamente a este participante'+
    				'</div>'
        		)
        	}
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }

    });
}

function imprimirCertificado(documento, idParticipanteEvento) {
	$.ajax({
        type:"GET",
        dataType:"html",
        url: baseUrl+"admin/participante/entregarCertificadoFisico/" + idParticipanteEvento,
        success:function(msg){
        	console.log(JSON.parse(msg));
        	if(JSON.parse(msg)==true){

        		if (!$( "#impresoEntregado" ).length ) {
	        		$('#despliegue').prepend(''+
	        			'<div id="impresoEntregado" class="ui warning message">'+
	    				  '<div class="header">' +
	    				   	'Certificado Entregado' +
	    				  '</div>'+
	    				  'El certificado impreso ya fue entregado previamente a este participante'+
	    				'</div>'
	        		)
        		}
        	}
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });


	var wnd = window.open(documento);
	//$.delay(10000);
    wnd.print();
}


function desplegarSolapin(idParticipanteEvento){

	$('#certificadoModal').remove();

	$('body').append('<div class="ui active dimmer"><div class="ui large text loader"></div></div>')

	$('body').append(''+
		'<div class="ui modal" id="certificadoModal" >'+
		 '<i class="close icon"></i>'+
		  '<div class="header">'+
		  	'<span style="float:right"> '+
		  		'<a title="Imprimir" href="#" onclick="imprimirSolapin(\''+ baseUrl + '/admin/participante/generarSolapin/' + idParticipanteEvento +'\' , '+ idParticipanteEvento + ')"><i class="ui blue print icon"></i><a>'+
		  	'</span>'+
		    'Solapín'+
		  '</div>'+
		  '<div id="despliegue" class="content">'+
		  '<iframe src="' + assetsUrl +'/js/pdfjs/web/viewer.html?file='+ baseUrl + '/admin/participante/generarSolapin/' + idParticipanteEvento + '" width="100%" height="600" ></iframe>' +
		  '</div>'+
		'</div>'
	);

	$('.dimmer').remove();

	$.ajax({
        type:"GET",
        dataType:"html",
        url: baseUrl+"admin/participante/revisarEntregaSolapin/" + idParticipanteEvento,
        success:function(msg){
        	console.log(JSON.parse(msg));
        	if(JSON.parse(msg)==true){
        		$('#despliegue').prepend(''+
        			'<div id="impresoEntregado" class="ui warning message">'+
    				  '<div class="header">' +
    				   	'Solapín Entregado' +
    				  '</div>'+
    				  'El solapín impreso ya fue entregado previamente a este participante'+
    				'</div>'
        		)
        	}
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }

    });

	$('#certificadoModal').modal('show');
}

function imprimirSolapin(documento, idParticipanteEvento){

	$.ajax({
        type:"GET",
        dataType:"html",
        url: baseUrl+"admin/participante/entregarSolapin/" + idParticipanteEvento,
        success:function(msg){
        	console.log(JSON.parse(msg));
        	if(JSON.parse(msg)==true){
        		if (!$( "#impresoEntregado" ).length ) {
	        		$('#despliegue').prepend(''+
	        			'<div id="impresoEntregado" class="ui warning message">'+
	    				  '<div class="header">' +
	    				   	'Solapín Entregado' +
	    				  '</div>'+
	    				  'El solapín impreso ya fue entregado previamente a este participante'+
	    				'</div>'
	        		)
        		}
        	}
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });


	var wnd = window.open(documento);
	wnd.onload(wnd.print());

}


function enviarCertificado(idParticipanteEvento){

	$('body').append('<div id="envioCorreo" class="ui active dimmer"><div class="ui large text loader"><i class="inverted mail icon"></i> Enviando Correo Por favor espere</div></div>')
	$.ajax({
        type:"GET",
        dataType:"html",
        url: baseUrl+"admin/participante/enviarCertificado/" + idParticipanteEvento,
        success:function(msg){
        	console.log(msg);
        	$('#envioCorreo').remove();
        	$('#mensajeEnvioCorreo').transition('fade','1000ms').delay(3000).transition('fade', '1000ms');
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });

}
