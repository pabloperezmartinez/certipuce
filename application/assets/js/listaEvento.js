$(document).ready(function(){
    $("#eventos").DataTable( {
    	"columnDefs": [
    	               { "orderable": false, "targets": 0 }
    	             ],
    	scrollY:        "60vh",
    	deferRender:    true,
        scrollCollapse: true,
        deferRender:    false,
        scroller:       true,
    	language: {
    		"sProcessing":     "Procesando...",
    	    "sLengthMenu":     "Mostrar _MENU_ registros",
    	    "sZeroRecords":    "No se encontraron resultados",
    	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    	    "sInfoPostFix":    "",
    	    "sSearch":         "<i class=\"search icon\"></i><div style='margin-top:20px;';></div>",
    	    "sUrl":            "",
    	    "sInfoThousands":  ",",
    	    "sLoadingRecords": "Cargando...",
    	    "oPaginate": {
    	        "sFirst":    "Primero",
    	        "sLast":     "Último",
    	        "sNext":     "Siguiente",
    	        "sPrevious": "Anterior"
    	    }
        }	
    });
});

function desplegarConfirmacionEliminacion(idEvento, tituloEvento){
	$('#eliminacionModal').remove();
	$('body').append(''+
			'<div class="ui basic modal" id="eliminacionModal">'+
			  '<i class="close icon"></i>'+
			  '<div class="header">'+
			    'Eliminar Evento'+
			  '</div>'+
			  '<div class="image content">'+
			    '<div class="image">'+
			      '<i class="red remove icon"></i>'+
			    '</div>'+
			    '<div class="description">'+
			      '<p>Está a punto de eliminar el evento: "' + tituloEvento + '"</p>'+
			      '<input type="hidden" name="idEventoEliminar" id="idEventoEliminar"/>'+
			      '<form class="ui form">'+
				      '<div class="ui required field">'+
				      	'<label style="color:#fff">Observaciones</label>'+
				      	'<textarea id="observacionesEliminacion"></textarea>'+
				      '</div>'+
			      '</form>'+
			    '</div>'+
			  '</div>'+
			  '<div class="actions">'+
			    '<div class="two fluid ui inverted buttons">'+
			      '<div class="ui red basic inverted button" onclick="$(\'#eliminacionModal\').modal(\'hide\')">'+
			        '<i class="remove icon"></i>'+
			        'No'+
			      '</div>'+
			      '<div class="ui green basic inverted button" onclick="eliminarEvento('+ idEvento +')">'+
			        '<i class="checkmark icon"></i>'+
			        'Sí'+
			      '</div>'+
			    '</div>'+
			  '</div>'+
			'</div>'
    	);
	$('#eliminacionModal').modal('show');
}

function eliminarEvento(idEvento){
	$.ajax({
        type:"POST",
        dataType:"html",
        url: baseUrl+"admin/evento/eliminarEvento/" + idEvento,
        data: { observacionesEliminacion: $('#observacionesEliminacion').val()},
        success:function(msg){
        	location.reload();
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });
}
//hhh