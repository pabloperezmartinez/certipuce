<?php
/**
 * Clase Participante
 * @author tics
 */

use Carbon\Carbon;
class Participante extends CI_Controller {

	/**
	 * Constructor de clase Participante
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('participante_model');
		$this->load->model('participanteevento_model');
		$this->load->model('tipoparticipante_model');
		$this->load->model('tipoformapago_model');
		$this->load->model('evento_model');
		$this->load->model('descuento_model');
		$this->load->model('pais_model');
		$this->load->model('auditoria_model');
		$this->load->helper('text');
	}


	/**
	 * Despliega los detalles de un participante
	 * @param int $idParticipante
	 */

	public function detalles($idParticipante, $idEvento){
		$data['idEvento']=$idEvento;
		if($this->session->userdata('idTipoEmpleado')!=null){
			$data['participante']=$this->participante_model->obtenerParticipante($idParticipante);
			if ($data['participante']['idDescuentoRegistrado']!=null && $data['participante']['idDescuentoRegistrado']!=0)
				$data['descuentoRegistrado']=$this->descuento_model->obtenerDescuento($data['participante']['idDescuentoRegistrado']);
			$data['pais']=$this->pais_model->obtenerPaises($data['participante']['idPais']);
			$data['datosFacturacion']=$this->participante_model->obtenerDatosFacturacion($idParticipante, $idEvento);
			$data['tipoParticipante']=$this->tipoparticipante_model->obtenerDescripcionTipoParticipante($data['datosFacturacion']['idTipoParticipante']);
			$data['formaPago']=$this->tipoformapago_model->obtenerDescripcionFormaPago($data['datosFacturacion']['idTipoFormaPago']);

			$data['titulo']="Detales de ".$data['participante']['nombresParticipante']." ".$data['participante']['apellidosParticipante'];
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('admin/participante/detalles', $data);
			if($this->session->userdata('idTipoEmpleado')!=1){
				if ($data['datosFacturacion']['estadoParticipanteEvento']==2)
					$this->load->view('admin/participante/formFinanciero', $data);
				else
					$this->load->view('admin/participante/financiero', $data);
			}
			$this->load->view('/templates/admin/footer');
		}else{
			$data['titulo']="Sin acceso";
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('templates/sinAcceso');
			$this->load->view('/templates/admin/footer');
		}
	}

	/**
	 * Genera un certificado o devuelve la ruta del mismo en el caso que ya se encuentre creado
	 * @param int $idParticipanteEvento
	 */

	public function generarCertificado($idParticipanteEvento){
		$participanteEvento = $this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$evento = $this->evento_model->obtenerEventos($participanteEvento['idEvento']);
		if ($participanteEvento['rutaCertificadoParticipanteEvento']==null || $participanteEvento['rutaCertificadoParticipanteEvento']==''){
			$plantillaEvento=FCPATH.'/application/cargasDocumentos/plantillasCertificado/'.$this->evento_model->obtenerPlantillaCertificado($participanteEvento['idEvento']);
			
			// Generar subdirectorios

			if (!file_exists('./application/cargasDocumentos/certificados/QR/'.date("Y"))) {
				mkdir('./application/cargasDocumentos/certificados/QR/'.date("Y"), 0777);
				chmod('./application/cargasDocumentos/certificados/QR/'.date("Y"), 0777);
			}
			if (!file_exists('./application/cargasDocumentos/certificados/DOCX/'.date("Y"))) {
				mkdir('./application/cargasDocumentos/certificados/DOCX/'.date("Y"), 0777);
				chmod('./application/cargasDocumentos/certificados/DOCX/'.date("Y"), 0777);
			}
			if (!file_exists('./application/cargasDocumentos/certificados/PDF/'.date("Y"))) {
				mkdir('./application/cargasDocumentos/certificados/PDF/'.date("Y"), 0777);
				chmod('./application/cargasDocumentos/certificados/PDF/'.date("Y"), 0777);
			}

			if (!file_exists('./application/cargasDocumentos/certificados/QR/'.date("Y").'/'.date('M'))) {
				mkdir('./application/cargasDocumentos/certificados/QR/'.date("Y").'/'.date('M'), 0777);
				chmod('./application/cargasDocumentos/certificados/QR/'.date("Y").'/'.date('M'), 0777);
			}

			if (!file_exists('./application/cargasDocumentos/certificados/DOCX/'.date("Y").'/'.date('M'))) {
				mkdir('./application/cargasDocumentos/certificados/DOCX/'.date("Y").'/'.date('M'), 0777);
				chmod('./application/cargasDocumentos/certificados/DOCX/'.date("Y").'/'.date('M'), 0777);
			}

			$path_anio_mes=date("Y").'/'.date('M').'/';

			// Generar Código QR
			$this->load->library('ciqrcode');

			$params['data'] = 'http://docs.google.com/gview?url='. base_url('application/cargasDocumentos/certificados/DOCX/'.$path_anio_mes.$participanteEvento['idParticipanteEvento'].'.docx');
			$params['level'] = QR_ECLEVEL_L;
			$params['size'] = 4;
			$params['savename'] = FCPATH.'application/cargasDocumentos/certificados/QR/'.$path_anio_mes.$participanteEvento['idParticipanteEvento'].'.png';
			$this->ciqrcode->generate($params);
			
			//Generar Word
			$this->load->library('word');
			$plantillaWord = new \PhpOffice\PhpWord\TemplateProcessor($plantillaEvento);
			$plantillaWord->setValue('nombreParticipante',$participanteEvento['certificadoParticipanteEvento']);
			$plantillaWord->setValue('tituloEvento',$evento['tituloEvento']);

			$fechaInicioEvento = new DateTime($evento['fechaInicioEvento']);
			$fechaInicioEvento = new Carbon($fechaInicioEvento->format(DATE_ISO8601));
			$fechaFinEvento = new DateTime($evento['fechaFinEvento']);
			$fechaFinEvento = new Carbon($fechaFinEvento->format(DATE_ISO8601));
			$fechaEmision = Carbon::now();

			$plantillaWord->setValue('fechaInicio',$fechaInicioEvento->formatLocalized('%d de %B de %Y'));
			$plantillaWord->setValue('fechaFin',$fechaFinEvento->formatLocalized('%d de %B de %Y'));
			$plantillaWord->setValue('fechaEmision',$fechaEmision->formatLocalized('%d de %B de %Y'));

			$plantillaWord->saveAs(FCPATH.'application/cargasDocumentos/certificados/DOCX/'.$path_anio_mes.'/'.$participanteEvento['idParticipanteEvento'].'.docx');

			$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->almacenarCertificado($participanteEvento['idParticipanteEvento'], $path_anio_mes.$participanteEvento['idParticipanteEvento'].'.docx')));

		}else{
			$this->output->set_content_type('application/json')->set_output(json_encode($participanteEvento['rutaCertificadoParticipanteEvento']));
		}
	}

	/**
	 * Actualiza todos los datos del participante enviados desde el formulario financiero
	 * @param int $idParticipante
	 */
	public function actualizarDatosFinancieros($idParticipanteEvento){
		$data['titulo']="Usuario Actualizado";
		$data['participanteEvento']=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$this->participanteevento_model->actualizarDatosFinancieros($idParticipanteEvento);
		if($this->input->post('estadoParticipanteEvento')==1){
			$this->auditoria_model->ingresarLog('Aprueba inscripción de partipante-evento '.$idParticipanteEvento);
			$this->notificarAceptacionInscripcion($idParticipanteEvento);
		}else {
			$this->auditoria_model->ingresarLog('Rechaza inscripción de partipante-evento '.$idParticipanteEvento);
			$this->notificarRechazoInscripcion($idParticipanteEvento);
		}
		$this->load->view('/templates/admin/header', $data);
		$this->load->view('admin/participante/actualizacionDatosFinanciero');
		$this->load->view('/templates/admin/footer');
	}

	/**
	 * Registra una bandera cuando el documento es entregado
	 * @param int $idParticipanteEvento
	 */
	public function entregarCertificadoFisico($idParticipanteEvento){
		$this->auditoria_model->ingresarLog('Entrega certificado físico de particiante-evento '.$idParticipanteEvento);
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->guardarEntregaCertificado($idParticipanteEvento)));
	}

	/**
	 * Devuelve e json el valor verdadero o falso para saber si un certificado ha sido entregado
	 * @param int $idParticipanteEvento
	 */

	public function revisarEntregaCertificado($idParticipanteEvento){
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->revisarEntregaCerficado($idParticipanteEvento)));
	}

	/**
	 * Despliega pdf para entrega de solapín
	 * @param int $idParticipanteEvento
	 */
	public function generarSolapin($idParticipanteEvento){
		//obtener participante de Evento
		$participanteEvento=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$participante=$this->participante_model->obtenerParticipante($participanteEvento['idParticipante']);
		$evento=$this->evento_model->obtenerEventos($participanteEvento['idEvento']);
		//Leer librería
		$this->load->library('pdf');
		// crear clase PDF
		$this->pdf = new Pdf();
		// Agregar fuente de barcode
		$this->pdf->AddFont('Code39FreeEdition','','c39_free.php');
		// Agregar una página
		$this->pdf->AddPage();

		$condicion=array("idParticipanteEvento"=>$idParticipanteEvento);
		$aCambiar=array("solapinParticipanteEvento"=>1);
		//$database->update("participanteevento",$aCambiar,$condicion);
		$nombres=utf8_decode(mb_strtoupper($participante['nombresParticipante'],'utf-8'));
		$apellidos=utf8_decode(mb_strtoupper($participante['apellidosParticipante'],'utf-8'));
		$evento=utf8_decode(mb_strtoupper($evento['tituloEvento'],'utf-8'));
		$id=utf8_decode(mb_strtoupper('*'.sprintf("%05s",$participanteEvento['idParticipanteEvento']),'utf-8').'*');
		$this->pdf->SetFont('Arial','B',18);
		$this->pdf->Ln(91);
		$this->pdf->Cell(72,10,$nombres,0,0,'C');
		$this->pdf->Ln(5);
		$this->pdf->Cell(72,10,$apellidos,0,0,'C');
		$this->pdf->Ln(4);
		$this->pdf->SetFont('Arial','B',10);
		$this->pdf->Cell(72,10,$evento,0,0,'C');
		$this->pdf->Ln(15);
		$this->pdf->SetFont('Code39FreeEdition','',40);
		$this->pdf->Cell(72,10,$id,0,0,'C');

		$this->pdf->Output();
	}

	/**
	 * Despliega un JSON con la respuesta verdadera o false de la entrega previa de un solapín
	 * @param int $idParticipanteEvento
	 */
	public function revisarEntregaSolapin($idParticipanteEvento){
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->revisarEntregaSolapin($idParticipanteEvento)));
	}

	/**
	 * Registra la entrega física del solapín
	 * @param int $idParticipanteEvento
	 */
	public function entregarSolapin($idParticipanteEvento){
		$this->auditoria_model->ingresarLog('Entrega solapín de partipante-evento '.$idParticipanteEvento);
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->guardarEntregaSolapin($idParticipanteEvento)));
	}

	/**
	 * Envía al email del participante el certificado del evento al que asistió
	 * @param int $idParticipanteEvento
	 */
	public function enviarCertificado($idParticipanteEvento){
		$this->auditoria_model->ingresarLog('Envía por email certificado de partipante-evento '.$idParticipanteEvento);
		$participanteEvento=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$data['evento']=$this->evento_model->obtenerEventos($participanteEvento['idEvento']);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($participanteEvento['idParticipante']);
		$data['qr']=base_url('application/cargasDocumentos/certificados/QR/'.str_replace('.pdf','.png',$participanteEvento['rutaCertificadoParticipanteEvento']));
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Centro José Pedro Varela: Sistema de Matriculas');
		$this->email->subject('Envío de certificado de evento: "'.$data['evento']['tituloEvento'].'"');
		$this->email->to($participante['correoElectronicoParticipante']);
		$this->email->attach(FCPATH.'application/cargasDocumentos/certificados/PDF/'.$participanteEvento['rutaCertificadoParticipanteEvento']);
		$data['emailParticipante']=$participante['correoElectronicoParticipante'];
		$mensaje=$this->load->view('emailTemplates/envioCertificado', $data, TRUE);
		$this->email->message($mensaje);
		$this->email->send();
		echo $this->email->print_debugger();
	}

	/**
	 * Envía por email cuando su inscripción ha sido aceptada a un evento
	 * @param int $idParticipanteEvento
	 */
	protected function notificarAceptacionInscripcion($idParticipanteEvento){
		$data['participanteEvento']=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($data['participanteEvento']['idParticipante']);
		$data['evento']=$this->evento_model->obtenerEventos($data['participanteEvento']['idEvento']);

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Centro José Pedro Varela: Sistema de Matriculas');
		$this->email->subject('Sistema de Matrículas: Inscripción Aceptada');
		$this->email->to($participante['correoElectronicoParticipante']);
		$mensaje=$this->load->view('emailTemplates/inscripcionAceptada', $data, TRUE);
		//echo $mensaje;
		$this->email->message($mensaje);
		$this->email->send();
		echo $this->email->print_debugger();
	}

	/**
	 * Envía por email cuando su inscripción ha sido rechazada a un evento
	 * @param int $idParticipanteEvento
	 */
	protected function notificarRechazoInscripcion($idParticipanteEvento){
		$data['participanteEvento']=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($data['participanteEvento']['idParticipante']);
		$data['evento']=$this->evento_model->obtenerEventos($data['participanteEvento']['idEvento']);

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Centro José Pedro Varela: Sistema de Matriculas');
		$this->email->subject('Sistema de Matrículas: Inscripción Rechazada');
		$this->email->to($participante['correoElectronicoParticipante']);
		$mensaje=$this->load->view('emailTemplates/inscripcionEliminada', $data, TRUE);
		//echo $mensaje;
		$this->email->message($mensaje);
		$this->email->send();
		echo $this->email->print_debugger();
	}

	/**
	 * Elimina los datos de descuento de un participante y retorna el valor original a pagar en los descuentos
	 * @param int $idParticipanteEvento
	 */
	public function rechazarDescuento($idParticipanteEvento){
		$observaciones=$this->input->post('observaciones');
		$this->notificarRechazoDescuento($idParticipanteEvento, $observaciones);
		$participanteEvento=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$participante=$this->participante_model->obtenerParticipante($participanteEvento['idParticipante']);
		$this->participante_model->quitarDescuento($participanteEvento['idParticipante']);
		$this->auditoria_model->ingresarLog('Elimina descuento tipo '. $participante['idDescuentoRegistrado'] .' de participante. OBSERVACIONES: '.$observaciones);
		$costoSinDescuento=$this->evento_model->obtenerValorTipoParticipante($participanteEvento['idEvento'], $idParticipanteEvento);
		$this->participanteevento_model->actualizarValorAPagar($idParticipanteEvento, $costoSinDescuento);
		$this->auditoria_model->ingresarLog('Elimina descuento de participante. OBSERVACIONES: '.$observaciones);

	}

	/**
	 * Notifica la NO aprobación de un descuento a un participante
	 * @param int $idParticipanteEvento
	 * @param string $observaciones
	 */
	protected function notificarRechazoDescuento($idParticipanteEvento, $observaciones){
		$data['participanteEvento']=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($data['participanteEvento']['idParticipante']);
		$data['evento']=$this->evento_model->obtenerEventos($data['participanteEvento']['idEvento']);
		$data['observaciones']=$observaciones;
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Centro José Pedro Varela: Sistema de Matriculas');
		$this->email->subject('Sistema de Matrículas: Descuento NO Aceptado');
		$this->email->to($participante['correoElectronicoParticipante']);
		$mensaje=$this->load->view('emailTemplates/rechazoDescuento', $data, TRUE);
		//echo $mensaje;
		$this->email->message($mensaje);
		$this->email->send();
		echo $this->email->print_debugger();
	}


	public function actualizarFacturacion($idParticipanteEvento){
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->actualizarFactura($idParticipanteEvento)));
	}
}
