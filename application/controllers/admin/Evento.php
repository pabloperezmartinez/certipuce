<?php

/**
 * Clase Evento
 * @author tics
 */
class Evento extends CI_Controller {

	/**
	 * Constructor de clase Evento
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('evento_model');
		$this->load->model('tipoevento_model');
		$this->load->model('tipoparticipante_model');
		$this->load->model('participante_model');
		$this->load->model('participanteevento_model');
		$this->load->model('auditoria_model');
		$this->load->model('empleado_model');
		$this->load->helper('text');
	}

	/**
	 * Despliegue de formulario de login de empleado
	 */
	public function index() {
		if($this->session->userdata('idTipoEmpleado')!=null){
			$data['titulo']="Eventos";
			$data['eventos']=$this->evento_model->obtenerEventos();
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('admin/evento/lista', $data);
			$this->load->view('/templates/admin/footer');
		}else{
			$data['titulo']="Sin acceso";
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('templates/sinAcceso');
			$this->load->view('/templates/admin/footer');
		}
	}


	/**
	 * Despliega formulario de creación de evento
	 * @uses this::notificarCreacionEvento
	 */
	public function crear() {
		if($this->session->userdata('idTipoEmpleado')!=null){
			$data['titulo']="Crear Evento";
			$data['tiposEvento']=$this->tipoevento_model->obtenerTiposEvento();
			$data['tiposParticipante']=$this->tipoparticipante_model->obtenerTiposParticipante();
			if ($this->input->post('nombreEvento')==null || $this->input->post('nombreEvento')==''){
				$this->load->view('/templates/admin/header', $data);
				$this->load->view('admin/evento/creacion', $data);
				$this->load->view('/templates/admin/footer');
			}else{
				$idEvento=$this->evento_model->insertarEvento();
				if($this->input->post('certificadoEvento')!=null){
					$config['upload_path']          ='./application/cargasDocumentos/plantillasCertificado/';
					$config['allowed_types'] 		= 'docx|doc';
					$config['encrypt_name'] 		= TRUE;
					$config['overwrite'] 			= FALSE;
					$this->load->library('upload');
					$this->upload->initialize($config);


					if($this->upload->do_upload('plantillaEvento')){
					$archivo = $this->upload->data();
					$nombreArchivo=$archivo['file_name'];

					$this->evento_model->ingresarPlantillaEvento($idEvento, $nombreArchivo);
					}
					else{
						$this->load->view('/templates/admin/header', $data);
						echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
						$this->load->view('/templates/admin/footer');
						die();
					}
				}

				if($_FILES['imagenEvento']!=null){
					$config['upload_path']          ='./application/cargasDocumentos/imagenes/';
					$config['allowed_types'] 		= 'jpg|jpeg|png|gif|bmp';
					$config['encrypt_name'] 		= TRUE;
					$config['overwrite'] 			= FALSE;
					$this->load->library('upload');
					$this->upload->initialize($config);

					if($this->upload->do_upload('imagenEvento')){
						$imagen = $this->upload->data();
						$nombreArchivo=$imagen['file_name'];

						$this->evento_model->ingresarImagenEvento($idEvento, $nombreArchivo);
					}
					else{
						$this->load->view('/templates/admin/header', $data);
						echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
						$this->load->view('/templates/admin/footer');
						die();
					}
				}


				$this->auditoria_model->ingresarLog("Crea el evento ". $idEvento);
				$this->notificarCreacionEvento($idEvento);
				redirect('admin/evento');
			}
		}else{
			$data['titulo']="Sin acceso";
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('templates/sinAcceso');
			$this->load->view('/templates/admin/footer');
		}

	}


	/**
	 * Obtiene los detalles y los participantes de un evento
	 * @param int $idEvento
	 */
	public function detalles($idEvento){
		$data['idEvento']=$idEvento;
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		if($this->session->userdata('idTipoEmpleado')!=null && $data['evento']){
			$data['costos']=$this->evento_model->obtenerCostos($idEvento);
			$data['participantes']=$this->participante_model->obtenerParticipanteEvento($idEvento);
			$data['titulo']="Detalles de ".$data['evento']['tituloEvento'];
			$data['numeroParticipantes']=$this->evento_model->obtenerNumeroParticipantesEvento($idEvento);
			$data['participantesConfirmados']=$this->evento_model->obtenerNumeroParticipantesConfirmados($idEvento);
			$data['aModificar']=(restarFechas(date("Y-m-d H:m:i"),$data['evento']['fechaFinEvento'])>0)?1:0;
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('admin/evento/detalles', $data);
			$this->load->view('/templates/admin/footer');
		}else{
			$data['titulo']="Sin acceso";
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('templates/sinAcceso');
			$this->load->view('/templates/admin/footer');
		}
	}

	/**
	 * Acepta en masa la inscripción de varios participantes
	 * @param int $idEvento
	 */
	public function aprobarParticipantesEvento($idEvento){
		$this->evento_model->aprobarParticipantesEvento($idEvento);
		redirect('admin/evento/detalles/'.$idEvento);
	}

	/**
	 * Cambia estado de evento a eliminado
	 * @param int $idEvento
	 */
	public function eliminarEvento($idEvento){
		$this->auditoria_model->ingresarLog("Elimina el evento ". $idEvento);
		$this->output->set_content_type('application/json')->set_output(json_encode($this->evento_model->cambiarEstadoEvento($idEvento,0, $this->session->userdata('idEmpleado'))));
	}


	/**
	 * Envía por email cuando la inscripción ha sido ELIMMINADA de un evento
	 * @param int $idParticipanteEvento
	 */
	protected function notificarEliminacionInscripcion($idParticipanteEvento){
		$data['participanteEvento']=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($data['participanteEvento']['idParticipante']);
		$data['evento']=$this->evento_model->obtenerEventos($data['participanteEvento']['idEvento']);

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Centro José Pedro Varela: Sistema de Matriculas');
		$this->email->subject('Sistema de Matrículas: Inscripción Eliminada');
		$this->email->to($participante['correoElectronicoParticipante']);
		$mensaje=$this->load->view('emailTemplates/inscripcionEliminada', $data, TRUE);
		//echo $mensaje;
		$this->email->message($mensaje);
		$this->email->send();
		echo $this->email->print_debugger();
	}

	/**
	 * Cambia estado de participante a eliminado
	 * @param int $idEvento
	 */
	public function eliminarParticipante($idParticipanteEvento){
		$this->notificarEliminacionInscripcion($idParticipanteEvento);
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participanteevento_model->cambiarEstado($idParticipanteEvento,0)));
		$this->auditoria_model->ingresarLog("Elimina de participanteevento ". $idParticipanteEvento);
	}

	/**
	 * Notifica a los usuarios acerca de un evento creado
	 * @param int $idEvento
	 */
	protected function notificarCreacionEvento($idEvento){
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		$empleados=$this->empleado_model->obtenerEmpleadoNoAdministrador();

		foreach ($empleados as $empleado){
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from('info@ccjpv.com', 'Sistema de Matriculas');
			$this->email->subject('Sistema de Matrículas: Creación de nuevo evento');
			$this->email->to($empleado['correoElectronicoEmpleado']);
			$data['empleado']=$empleado;
			$mensaje=$this->load->view('emailTemplates/creacionEvento', $data, TRUE);
			//echo $mensaje;
			$this->email->message($mensaje);
			$this->email->send();
			echo $this->email->print_debugger();
		}
	}

	/**
	 * Modifica Datos de Evento
	 * @param int $idEvento
	 */
	public function modificarEvento($idEvento,$actualizar=0){

		if (is_numeric($idEvento)){
			$evento=$data['evento']=$this->evento_model->obtenerEventos($idEvento);
			if ($actualizar){
				$datos = array(
					'tituloEvento' => preg_replace('#<[^>]+>#', ' ',$this->input->post('nombreEvento')),
					'idTipoEvento' => $this->input->post('tipoEvento'),
					'fechaInicioEvento' => $this->input->post('fechaInicioEvento'),
					'fechaFinEvento' => $this->input->post('fechaFinEvento'),
					'descripcionEvento' => $this->input->post('descripcionEvento'),
					'certificadoEvento' => 0,
					'observacionesModificacion' => $this->input->post('observacionesModificacion'),
					'cupoEvento' => $this->input->post('cupoEvento')
				);
				if ($this->input->post('certificadoEvento') != null) {
					$config['upload_path']          ='./application/cargasDocumentos/plantillasCertificado/';
					$config['allowed_types'] 		= 'docx|doc';
					$config['encrypt_name'] 		= TRUE;
					$config['overwrite'] 			= TRUE;
					$this->load->library('upload', $config);
					if($this->upload->do_upload('plantillaEvento')){
						$archivo = $this->upload->data();
						$nombreArchivo=$archivo['file_name'];
						$datos['certificadoEvento']=1;
						$datos['plantillaCertificadoEvento']=$nombreArchivo;
					}
					else{
						$this->load->view('/templates/admin/header', $data);
						echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
						$this->load->view('/templates/admin/footer');
					}
				}
				if( isset($_FILES['imagenEvento']['name']) && $_FILES['imagenEvento']['name'] != null){
					echo "entra";
					$config['upload_path']          ='./application/cargasDocumentos/imagenes/';
					$config['allowed_types'] 		= 'jpg|jpeg|png|gif|bmp';
					$config['encrypt_name'] 		= TRUE;
					$config['overwrite'] 			= FALSE;
					$this->load->library('upload');
					$this->upload->initialize($config);

					if($this->upload->do_upload('imagenEvento')){
						$imagen = $this->upload->data();
						$nombreArchivo=$imagen['file_name'];

						$this->evento_model->ingresarImagenEvento($idEvento, $nombreArchivo);
					}
					else{
						$this->load->view('/templates/admin/header', $data);
						echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
						$this->load->view('/templates/admin/footer');
						die();
					}
				}
				$this->notificarModificacion($idEvento);
				$this->evento_model->actualizarEvento($idEvento,$datos);
				$this->auditoria_model->ingresarLog("Modifica evento ". $idEvento);
				redirect(base_url('/admin/evento/'));
			}
			else{
				$fecha = date_create($data['evento']['fechaInicioEvento']);
				$data['evento']['fechaInicioEvento']=date_format($fecha, 'Y/m/d H:i');
				$fecha = date_create($data['evento']['fechaFinEvento']);
				$data['evento']['fechaFinEvento']=date_format($fecha, 'Y/m/d H:i');
				if(($this->session->userdata('idTipoEmpleado')==1 || $this->session->userdata('idTipoEmpleado')==2)&& $data['evento']){
					if ($data['evento']['plantillaCertificadoEvento'])
						$data['evento']['miniatura']= obtieneImagen($data['evento']['plantillaCertificadoEvento']);
					$data['tiposParticipante']=$this->evento_model->obtenerCostos($idEvento);
					$data['tiposEvento']=$this->tipoevento_model->obtenerTiposEvento();
					$data['titulo']="Modificar Evento";
					$this->load->view('/templates/admin/header', $data);
					$this->load->view('admin/evento/modificar',$data);
					$this->load->view('/templates/admin/footer');
				}
				else{
					$data['titulo']="Sin acceso";
					$this->load->view('/templates/admin/header', $data);
					$this->load->view('templates/sinAcceso');
					$this->load->view('/templates/admin/footer');
				}
			}
		}
		else{
			$data['titulo']="Sin acceso";
			$this->load->view('/templates/admin/header', $data);
			$this->load->view('templates/sinAcceso');
			$this->load->view('/templates/admin/footer');
		}
	}

	/**
	 * Envío de notificación de modificación de evento a participantes inscritos en el mismo
	 * @param int $idEvento
	 */
	protected function notificarModificacion($idEvento){
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		$participantes=$this->participante_model->obtenerParticipanteEvento($idEvento);
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Sistema de Matriculas');
		$this->email->subject("SISTEMA DE MATRÍCULAS: Modificación en Evento \"". $data['evento']['tituloEvento'] ."\"");
		foreach ($participantes as $participante){
			$this->email->to($participante['correoElectronicoParticipante']);
			$data['participante']=$participante;
			$mensaje=$this->load->view('emailTemplates/notificarModificacion', $data, TRUE);
			$this->email->message($mensaje);
			$this->email->send();
			//echo $this->email->print_debugger();
		}
    }

    /**
     * Notifica la eliminación de un evento
     * @param int $idEvento
     */
    protected function notificarEliminacion($idEvento){
    	$data['evento']=$this->evento_model->obtenerEventos($idEvento);
    	$participantes=$this->participante_model->obtenerParticipanteEvento($idEvento);
    	$this->load->library('email');
    	$config['protocol'] = 'sendmail';
    	$config['mailpath'] = '/usr/sbin/sendmail';
    	$this->email->initialize($config);
    	$this->email->set_mailtype("html");
    	$this->email->from('info@ccjpv.com', 'Sistema de Matriculas');
    	$this->email->subject("SISTEMA DE MATRÍCULAS: Cancelación de Evento \"". $data['evento']['tituloEvento'] ."\"");
    	foreach ($participantes as $participante){
    		$this->email->to($participante['correoElectronicoParticipante']);
    		$data['participante']=$participante;
    		$mensaje=$this->load->view('emailTemplates/eliminacionEvento', $data, TRUE);
    		$this->email->message($mensaje);
    		$this->email->send();
    		echo $this->email->print_debugger();
    	}
    }
}
