<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evento extends CI_Controller{

  /**
   * Constructor
   */
  public function __construct()
  {
    parent::__construct();
    $this->load->model('evento_model');
  }

  /**
   * Obtiene los cuatro eventos más cercanos a la fecha actual
   * @return [type] [description]
   */
  function obtenerUltimosEventos()
  {
    $eventos = $this->evento_model->obtenerCuatroProximosEventos();
    foreach ($eventos as &$evento) {
      $evento['url'] = base_url('/evento/'.$evento['idEvento']);
      $evento['imagenEvento'] = base_url('/application/cargasDocumentos/imagenes/'.$evento['imagenEvento']);
      $fechaInicioEvento = new DateTime($evento['fechaInicioEvento']);
      $fechaInicioEvento = new Carbon\Carbon($fechaInicioEvento->format(DATE_ISO8601));
      $evento['fechaInicioEvento'] = $fechaInicioEvento->formatLocalized('%A, %d de %B de %Y');
      $fechaFinEvento = new DateTime($evento['fechaFinEvento']);
      $fechaFinEvento = new Carbon\Carbon($fechaFinEvento->format(DATE_ISO8601));
      $evento['fechaFinEvento'] = $fechaFinEvento->formatLocalized('%A, %d de %B de %Y');
      $evento['costoEvento'] = $this->evento_model->obtenerCostos($evento['idEvento'])[0]['costoValorEvento'];
    }
    echo json_encode($eventos);
  }

}
