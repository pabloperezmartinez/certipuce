<?php

/**
 * Clase Evento
 * @author tics
 */
class Evento extends CI_Controller {
	/**
	 * Constructor de clase Empleado
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('evento_model');
		$this->load->model('tipoevento_model');
		$this->load->model('tipoparticipante_model');
		$this->load->model('participanteevento_model');
		$this->load->model('tipoidentificacion_model');
		$this->load->model('participante_model');
		$this->load->model('pais_model');
		$this->load->helper('text');
	}

	/**
	 * Página de inicio de Evento
	 * @return void
	 */
	public function index(){
		$data['titulo']="Sistema Administrador de Matrículas - Eventos";
		$data['eventos']=$this->evento_model->obtenerProximosEventos();
		$data['eventos_en_curso']=$this->evento_model->obtenerEventosEnCurso();
		$this->load->view('templates/header', $data);
		$this->load->view('evento/index', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * Inscribe a un usuario específico en un evento específico
	 * TODO: validar que participante no esté inscrito en este evento
	 */

	public function inscribir()
	{
		if($this->session->userdata('idParticipante')!=null){
			if(!$this->participanteevento_model->comprobarParticipanteEvento($this->session->userdata('idParticipante') , $this->session->userdata('idEvento')))
			{
				$data['evento'] = $this->evento_model->obtenerEventos($this->session->userdata('idEvento'));
				$data['participante']=$this->participante_model->obtenerParticipante($this->session->userdata('idParticipante'));
				$data['descripcionTipoIdentificacion']=$this->tipoidentificacion_model->obtenerDescripcionTipoIdentificacion($data['participante']['idTipoIdentificacion']);
				$data['titulo']="Detalles de ".$data['evento']['tituloEvento'];

				if ($this->input->post('idTipoParticipante')==null) {
					$this->load->view('templates/header', $data);
					if ($this->participante_model->obtenerDatosFacturacion($this->session->userdata('idParticipante'), $this->session->userdata('idEvento'))==null){
						$this->load->view('/inscripcion/usuarioExistente', $data);}
					else {
						$this->load->view('/inscripcion/inscripcionPreviamenteRealizada', $data);
					}
					$this->load->view('templates/footer');

				} else {
					$idParticipanteEvento=$this->evento_model->agregarParticipante($this->input->post('idEvento'),$this->session->userdata('idParticipante'));
							$this->load->view('templates/header', $data);
							echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
							$this->load->view('templates/footer');
						}
					
					$this->notificarInscripcionParticipanteRegistrado($idParticipanteEvento);
					$this->load->view('templates/header', $data);
					$this->load->view('/inscripcion/inscripcionExitosa', $data);
					$this->load->view('templates/footer');
				} else {
				$data['titulo']="Usuario ya inscrito en evento";
				$this->load->view('templates/header', $data);
				$this->load->view('templates/yaInscrito');
				$this->load->view('templates/footer');
			}
		} else {
			$data['titulo']="Sin acceso";
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sinAcceso');
			$this->load->view('templates/footer');
		}

		$data['titulo']="Inscripción a evento";
	
	}
	/**
	 * Registra un usuario nuevo y lo inscribe en el evento
	 * @param int $i
	 */
	public function inscribirRegistro($idEvento) {
		$data['titulo']="Registro de nuevo participante";
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		$data['paises']=$this->pais_model->obtenerPaises();
		$data['tiposIdentificacion']=$this->tipoidentificacion_model->obtenerTiposIdentificacion();

		if ($this->input->post('nombresParticipante')==null){
			$this->load->view('templates/header', $data);
			$this->load->view('/inscripcion/usuarioNuevo', $data);
			$this->load->view('templates/footer');
		}else{
			$idParticipante=$this->participante_model->crearParticipante();
			$idParticipanteEvento=$this->evento_model->agregarParticipante($idEvento,$idParticipante);
			$this->load->view('templates/header', $data);
			echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
			$this->load->view('templates/footer');
			}
			
			$this->notificarInscripcionNuevoParticipante($idParticipanteEvento, $this->input->post('contrasenaParticipante'));
			$this->load->view('templates/header', $data);
			$this->load->view('/inscripcion/inscripcionExitosa', $data);
			$this->load->view('templates/footer');
			}

	/**
	 * Notifica a un participante previamente inscrito cuando completa el formulario correctamente
	 * @param int $idParticipanteEvento
	 */
	protected function notificarInscripcionParticipanteRegistrado($idParticipanteEvento){
		$data['participanteEvento']=$participanteEvento=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$data['evento']=$this->evento_model->obtenerEventos($participanteEvento['idEvento']);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($participanteEvento['idParticipante']);
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from('info@ccjpv.com', 'Pontificia Universidad Catolica del Ecuador');
			$this->email->subject('Confirmación de inscripción a evento: "'.$data['evento']['tituloEvento'].'"');
			$this->email->to($participante['correoElectronicoParticipante']);
			$mensaje=$this->load->view('emailTemplates/inscripcionParticipanteRegistrado', $data, TRUE);
			//echo $mensaje;
			$this->email->message($mensaje);
			$this->email->send();
			//echo $this->email->print_debugger();
	}
}