-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: matriculas
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asistencia`
--

DROP TABLE IF EXISTS `asistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistencia` (
  `idAsistencia` int(11) NOT NULL AUTO_INCREMENT,
  `fechaAsistencia` datetime NOT NULL,
  `idParticipanteEvento` int(11) NOT NULL,
  `idEmpleado` int(11) NOT NULL,
  PRIMARY KEY (`idAsistencia`),
  KEY `fk_asistencia_participanteevento1_idx` (`idParticipanteEvento`),
  KEY `fk_asistencia_empleado1_idx` (`idEmpleado`),
  CONSTRAINT `fk_asistencia_empleado1` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asistencia_participanteevento1` FOREIGN KEY (`idParticipanteEvento`) REFERENCES `participanteevento` (`idParticipanteEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistencia`
--

LOCK TABLES `asistencia` WRITE;
/*!40000 ALTER TABLE `asistencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `asistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria` (
  `fecha` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `idEmpleado` int(11) NOT NULL,
  `accion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` VALUES ('2018-04-13 23:02:48',3,'Crea el evento 2'),('2018-04-13 23:03:12',3,'Elimina el evento 1'),('2018-04-13 23:03:17',3,'Elimina el evento 1'),('2018-04-13 23:03:26',3,'Elimina el evento 1'),('2018-04-13 23:04:05',3,'Elimina el evento 1'),('2018-04-13 23:04:18',3,'Elimina el evento 1'),('2018-04-13 23:04:38',3,'Elimina el evento 1'),('2018-04-13 23:06:56',3,'Elimina el evento 1'),('2018-04-13 23:07:13',3,'Elimina el evento 1'),('2018-04-13 23:07:54',3,'Elimina el evento 2'),('2018-04-13 23:08:00',3,'Elimina el evento 2'),('2018-04-13 23:08:09',3,'Elimina el evento 2'),('2018-04-13 23:13:49',3,'Elimina el evento 2'),('2018-04-13 23:14:14',3,'Elimina el evento 1'),('2018-04-13 23:17:08',3,'Crea el evento 3'),('2018-04-13 23:22:56',3,'Aprueba inscripción de partipante-evento 1'),('2018-04-13 23:26:03',3,'Aprueba inscripción de partipante-evento 1'),('2018-04-14 00:09:09',3,'Aprueba inscripción de partipante-evento 7'),('2018-04-29 18:16:46',3,'Crea el evento 4'),('2018-04-29 18:22:10',3,'Aprueba inscripción de partipante-evento 5'),('2018-04-29 21:44:26',3,'Envía por email certificado de partipante-evento 5'),('2018-04-29 21:59:58',3,'Entrega certificado físico de particiante-evento 5'),('2018-04-29 22:00:23',3,'Entrega certificado físico de particiante-evento 5'),('2018-04-29 22:16:01',3,'Crea el evento 5'),('2018-04-29 22:22:44',3,'Aprueba inscripción de partipante-evento 7'),('2018-04-29 22:31:59',3,'Entrega certificado físico de particiante-evento 5'),('2018-04-29 22:32:40',3,'Aprueba inscripción de partipante-evento 6'),('2018-05-01 20:56:01',3,'Crea el evento 6'),('2018-05-01 20:58:47',3,'Crea el evento 7'),('2018-05-01 21:13:58',3,'Modifica evento 7');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datosfacturacion`
--

DROP TABLE IF EXISTS `datosfacturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datosfacturacion` (
  `idDatosFacturacion` int(11) NOT NULL AUTO_INCREMENT,
  `rucDatosFacturacion` varchar(45) NOT NULL DEFAULT '-',
  `razonSocialDatosFacturacion` varchar(250) NOT NULL,
  `direccionDatosFacturacion` varchar(150) NOT NULL DEFAULT 'Sin Dirección',
  `telefonoDatosFacturacion` varchar(30) NOT NULL DEFAULT '-',
  `estadoDatosFacturacion` tinyint(1) NOT NULL DEFAULT '1',
  `idParticipante` int(11) NOT NULL,
  PRIMARY KEY (`idDatosFacturacion`),
  KEY `fk_datosfacturacion_participante1_idx` (`idParticipante`),
  CONSTRAINT `fk_datosfacturacion_participante1` FOREIGN KEY (`idParticipante`) REFERENCES `participante` (`idParticipante`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datosfacturacion`
--

LOCK TABLES `datosfacturacion` WRITE;
/*!40000 ALTER TABLE `datosfacturacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `datosfacturacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descuento`
--

DROP TABLE IF EXISTS `descuento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descuento` (
  `idDescuento` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionDescuento` varchar(100) NOT NULL,
  `tazaDescuento` decimal(4,2) NOT NULL,
  PRIMARY KEY (`idDescuento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descuento`
--

LOCK TABLES `descuento` WRITE;
/*!40000 ALTER TABLE `descuento` DISABLE KEYS */;
INSERT INTO `descuento` VALUES (1,'Adulto Mayor',0.25),(2,'Estudiante',0.10),(3,'Discapacidad',0.15),(4,'Grupal',0.20);
/*!40000 ALTER TABLE `descuento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL AUTO_INCREMENT,
  `apellidosEmpleado` varchar(100) NOT NULL,
  `nombresEmpleado` varchar(100) NOT NULL,
  `correoElectronicoEmpleado` varchar(100) NOT NULL,
  `contrasenaEmpleado` varchar(100) NOT NULL,
  `estadoEmpleado` tinyint(1) NOT NULL DEFAULT '1',
  `idTipoEmpleado` int(11) NOT NULL,
  PRIMARY KEY (`idEmpleado`),
  KEY `fk_empleado_tipoempleado1_idx` (`idTipoEmpleado`),
  CONSTRAINT `fk_empleado_tipoempleado1` FOREIGN KEY (`idTipoEmpleado`) REFERENCES `tipoempleado` (`idTipoEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'CCJPV','Info','info@certipuce.test','21232f297a57a5a743894a0e4a801fc3',1,2);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `idEvento` int(11) NOT NULL AUTO_INCREMENT,
  `tituloEvento` varchar(100) NOT NULL,
  `descripcionEvento` longtext,
  `fechaInicioEvento` datetime NOT NULL DEFAULT '1900-01-01 01:01:01',
  `fechaFinEvento` datetime NOT NULL DEFAULT '1900-01-01 01:01:01',
  `imagenEvento` varchar(300) DEFAULT NULL,
  `certificadoEvento` tinyint(1) NOT NULL DEFAULT '1',
  `idPadreEvento` tinyint(1) NOT NULL DEFAULT '0',
  `cupoEvento` int(11) NOT NULL DEFAULT '10000',
  `observacionesEvento` varchar(255) DEFAULT NULL,
  `estadoEvento` tinyint(1) NOT NULL DEFAULT '1',
  `plantillaCertificadoEvento` varchar(100) DEFAULT NULL,
  `idTipoEvento` int(11) NOT NULL,
  `idEmpleadoCreacion` int(11) DEFAULT NULL,
  `observacionesModificacion` longtext,
  `idEmpleadoEliminacion` int(11) DEFAULT NULL,
  `observacionesEliminacion` longtext,
  PRIMARY KEY (`idEvento`),
  KEY `fk_evento_tipoevento1_idx` (`idTipoEvento`),
  KEY `fk_evento_empleado` (`idEmpleadoCreacion`),
  KEY `fk_empleadoeliminacion` (`idEmpleadoEliminacion`),
  CONSTRAINT `fk_empleadoeliminacion` FOREIGN KEY (`idEmpleadoEliminacion`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_empleado` FOREIGN KEY (`idEmpleadoCreacion`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_tipoevento1` FOREIGN KEY (`idTipoEvento`) REFERENCES `tipoevento` (`idTipoEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `idPais` int(11) NOT NULL AUTO_INCREMENT,
  `iso_a2` varchar(2) DEFAULT NULL,
  `iso_a3` varchar(3) DEFAULT NULL,
  `iso_n3` varchar(3) DEFAULT NULL,
  `fips` varchar(10) DEFAULT NULL,
  `descripcionPais` varchar(64) NOT NULL,
  `descripcionInglesPais` varchar(64) NOT NULL,
  `estadoPais` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'AD','AND','020','AN','Andorra','Andorra',1),(2,'AE','ARE','784','AE','Emiratos Árabes Unidos','United Arab Emirates',1),(3,'AF','AFG','004','AF','Afganistán','Afghanistan',1),(4,'AG','ATG','028','AC','Antigua y Barbuda','Antigua and Barbuda',1),(5,'AI','AIA','660','AV','Anguila','Anguilla',1),(6,'AL','ALB','008','AL','Albania','Albania',1),(7,'AM','ARM','051','AM','Armenia','Armenia',1),(8,'AO','AGO','024','AO','Angola','Angola',1),(9,'AQ','ATA','010','AY','Antártida','Antarctica',1),(10,'AR','ARG','032','AR','Argentina','Argentina',1),(11,'AS','ASM','016','AQ','Samoa Americana','American Samoa',1),(12,'AT','AUT','040','AU','Austria','Austria',1),(13,'AU','AUS','036','AS','Australia','Australia',1),(14,'AW','ABW','533','AA','Aruba','Aruba',1),(15,'AX','ALA','248',NULL,'Islas Áland','Åland Islands',1),(16,'AZ','AZE','031','AJ','Azerbaiyán','Azerbaijan',1),(17,'BA','BIH','070','BK','Bosnia y Herzegovina','Bosnia and Herzegovina',1),(18,'BB','BRB','052','BB','Barbados','Barbados',1),(19,'BD','BGD','050','BG','Bangladesh','Bangladesh',1),(20,'BE','BEL','056','BE','Bélgica','Belgium',1),(21,'BF','BFA','854','UV','Burkina Faso','Burkina Faso',1),(22,'BG','BGR','100','BU','Bulgaria','Bulgaria',1),(23,'BH','BHR','048','BA','Bahrein','Bahrain',1),(24,'BI','BDI','108','BY','Burundi','Burundi',1),(25,'BJ','BEN','204','BN','Benin','Benin',1),(26,'BL','BLM','652','TB','San Bartolomé','Saint Barthélemy',1),(27,'BM','BMU','060','BD','Bermuda','Bermuda',1),(28,'BN','BRN','096','BX','Brunei Darussalam','Brunei Darussalam',1),(29,'BO','BOL','068','BL','Bolivia','Bolivia',1),(30,'BQ','BES','535',NULL,'Bonaire, San Eustaquio y Saba','Bonaire, Sint Eustatius and Saba',1),(31,'BR','BRA','076','BR','Brasil','Brazil',1),(32,'BS','BHS','044','BF','Bahamas','Bahamas',1),(33,'BT','BTN','064','BT','Bután','Bhutan',1),(34,'BV','BVT','074','BV','Isla Bouvet','Bouvet Island',1),(35,'BW','BWA','072','BC','Botswana','Botswana',1),(36,'BY','BLR','112','BO','Belarús','Belarus',1),(37,'BZ','BLZ','084','BH','Belice','Belize',1),(38,'CA','CAN','124','CA','Canadá','Canada',1),(39,'CC','CCK','166','CK','Islas Cocos (Keeling)','Cocos (Keeling) Islands',1),(40,'CD','COD','180','CG','Congo (Rep. Democrática)','Congo Dem. Rep of the',1),(41,'CF','CAF','140','CT','República Centroafricana','Central African Republic',1),(42,'CG','COG','178','CF','Congo (Brazzaville)','Congo, Republic of',1),(43,'CH','CHE','756','SZ','Suiza','Switzerland',1),(44,'CI','CIV','384','IV','Costa de Marfil','Côte d\'Ivoire',1),(45,'CK','COK','184','CW','Islas Cook','Cook Islands',1),(46,'CL','CHL','152','CI','Chile','Chile',1),(47,'CM','CMR','120','CM','Camerún','Cameroon',1),(48,'CN','CHN','156','CH','China','China',1),(49,'CO','COL','170','CO','Colombia','Colombia',1),(50,'CR','CRI','188','CS','Costa Rica','Costa Rica',1),(51,'CU','CUB','192','CU','Cuba','Cuba',1),(52,'CV','CPV','132','CV','Cabo Verde','Cape Verde',1),(53,'CW','CUW','531','UC','Curaçao','Curaçao',1),(54,'CX','CXR','162','KT','Isla de Navidad','Christmas Island',1),(55,'CY','CYP','196','CY','Chipre','Cyprus',1),(56,'CZ','CZE','203','EZ','República Checa','Czech Republic',1),(57,'DE','DEU','276','GM','Alemania','Germany',1),(58,'DJ','DJI','262','DJ','Yibuti','Djibouti',1),(59,'DK','DNK','208','DA','Dinamarca','Denmark',1),(60,'DM','DMA','212','DO','Dominica','Dominica',1),(61,'DO','DOM','214','DR','República Dominicana','Dominican Republic',1),(62,'DZ','DZA','012','AG','Argelia','Algeria',1),(63,'EC','ECU','218','EC','Ecuador','Ecuador',1),(64,'EE','EST','233','EN','Estonia','Estonia',1),(65,'EG','EGY','818','EG','Egipto','Egypt',1),(66,'EH','ESH','732','WI','Sáhara Occidental','Western Sahara',1),(67,'ER','ERI','232','ER','Eritrea','Eritrea',1),(68,'ES','ESP','724','SP','España','Spain',1),(69,'ET','ETH','231','ET','Etiopía','Ethiopia',1),(70,'FI','FIN','246','FI','Finlandia','Finland',1),(71,'FJ','FJI','242','FJ','Fiyi','Fiji',1),(72,'FK','FLK','238','FK','Islas Malvinas (Falkland)','Falkland Islands (Malvinas)',1),(73,'FM','FSM','583','FM','Micronesia','Micronesia, Fed. States of',1),(74,'FO','FRO','234','FO','Islas Feroe','Faroe Islands',1),(75,'FR','FRA','250','FR','Francia','France',1),(76,'GA','GAB','266','GB','Gabón','Gabon',1),(77,'GB','GBR','826','UK','Reino Unido','United Kingdom',1),(78,'GD','GRD','308','GJ','Granada','Grenada',1),(79,'GE','GEO','268','GG','Georgia','Georgia',1),(80,'GF','GUF','254','FG','Guayana Francesa','French Guiana',1),(81,'GG','GGY','831','GK','Guernsey','Guernsey',1),(82,'GH','GHA','288','GH','Ghana','Ghana',1),(83,'GI','GIB','292','GI','Gibraltar','Gibraltar',1),(84,'GL','GRL','304','GL','Groenlandia','Greenland',1),(85,'GM','GMB','270','GA','Gambia','Gambia',1),(86,'GN','GIN','324','GV','Guinea','Guinea',1),(87,'GP','GLP','312','GP','Guadalupe','Guadeloupe',1),(88,'GQ','GNQ','226','EK','Guinea Ecuatorial','Equatorial Guinea',1),(89,'GR','GRC','300','GR','Grecia','Greece',1),(90,'GS','SGS','239','SX','Georgia del Sur e Islas Sandwich','South Georgia and the South Sandwich Islands',1),(91,'GT','GTM','320','GT','Guatemala','Guatemala',1),(92,'GU','GUM','316','GQ','Guam','Guam',1),(93,'GW','GNB','624','PU','Guinea-Bissau','Guinea-Bissau',1),(94,'GY','GUY','328','GY','Guayana','Guyana',1),(95,'HK','HKG','344','HK','Hong Kong','Hong Kong',1),(96,'HM','HMD','334','HM','Islas Heard y McDonald','Heard and McDonald Islands',1),(97,'HN','HND','340','HO','Honduras','Honduras',1),(98,'HR','HRV','191','HR','Croacia','Croatia',1),(99,'HT','HTI','332','HA','Haití','Haiti',1),(100,'HU','HUN','348','HU','Hungría','Hungary',1),(101,'ID','IDN','360','ID','Indonesia','Indonesia',1),(102,'IE','IRL','372','EI','Irlanda','Ireland',1),(103,'IL','ISR','376','IS','Israel','Israel',1),(104,'IM','IMN','833','IM','Isla de Man','Isle of Man',1),(105,'IN','IND','356','IN','India','India',1),(106,'IO','IOT','086','IO','Territorio Británico del Océano Índico','British Indian Ocean Territory',1),(107,'IQ','IRQ','368','IZ','Irak','Iraq',1),(108,'IR','IRN','364','IR','Irán','Iran',1),(109,'IS','ISL','352','IC','Islandia','Iceland',1),(110,'IT','ITA','380','IT','Italia','Italy',1),(111,'JE','JEY','832','JE','Jersey','Jersey',1),(112,'JM','JAM','388','JM','Jamaica','Jamaica',1),(113,'JO','JOR','400','JO','Jordania','Jordan',1),(114,'JP','JPN','392','JA','Japón','Japan',1),(115,'KE','KEN','404','KE','Kenia','Kenya',1),(116,'KG','KGZ','417','KG','Kirguistán','Kyrgyzstan',1),(117,'KH','KHM','116','CB','Camboya','Cambodia',1),(118,'KI','KIR','296','KR','Kiribati','Kiribati',1),(119,'KM','COM','174','CN','Islas Comoros','Comoros',1),(120,'KN','KNA','659','SC','Saint Kitts y Nevis','Saint Kitts and Nevis',1),(121,'KP','PRK','408','KN','Corea del Norte','Korea, Democratic People\'s Republic of',1),(122,'KR','KOR','410','KS','Corea del Sur','Korea, Republic of',1),(123,'KW','KWT','414','KU','Kuwait','Kuwait',1),(124,'KY','CYM','136','CJ','Islas Caimán','Cayman Islands',1),(125,'KZ','KAZ','398','KZ','Kazajstán','Kazakhstan',1),(126,'LA','LAO','418','LA','Laos','Lao, PDR',1),(127,'LB','LBN','422','LE','Líbano','Lebanon',1),(128,'LC','LCA','662','ST','Santa Lucía','Saint Lucia',1),(129,'LI','LIE','438','LS','Liechtenstein','Liechtenstein',1),(130,'LK','LKA','144','CE','Sri Lanka','Sri Lanka',1),(131,'LR','LBR','430','LI','Liberia','Liberia',1),(132,'LS','LSO','426','LT','Lesoto','Lesotho',1),(133,'LT','LTU','440','LH','Lituania','Lithuania',1),(134,'LU','LUX','442','LU','Luxemburgo','Luxembourg',1),(135,'LV','LVA','428','LG','Letonia','Latvia',1),(136,'LY','LBY','434','LY','Libia','Libya',1),(137,'MA','MAR','504','MO','Marruecos','Morocco',1),(138,'MC','MCO','492','MN','Mónaco','Monaco',1),(139,'MD','MDA','498','MD','Moldova','Moldova, Republic of',1),(140,'ME','MNE','499','MJ','Montenegro','Montenegro',1),(141,'MF','MAF','663','RN','San Martín','Saint Martin',1),(142,'MG','MDG','450','MA','Madagascar','Madagascar',1),(143,'MH','MHL','584','RM','Islas Marshall','Marshall Islands',1),(144,'MK','MKD','807','MK','Macedonia','Macedonia',1),(145,'ML','MLI','466','ML','Malí','Mali',1),(146,'MM','MMR','104','BM','Myanmar','Myanmar',1),(147,'MN','MNG','496','MG','Mongolia','Mongolia',1),(148,'MO','MAC','446','MC','Macao','Macau',1),(149,'MP','MNP','580','CQ','Islas Marianas del Norte','Northern Mariana Islands',1),(150,'MQ','MTQ','474','MB','Martinica','Martinique',1),(151,'MR','MRT','478','MR','Mauritania','Mauritania',1),(152,'MS','MSR','500','MH','Montserrat','Montserrat',1),(153,'MT','MLT','470','MT','Malta','Malta',1),(154,'MU','MUS','480','MP','Mauricio','Mauritius',1),(155,'MV','MDV','462','MV','Maldivas','Maldives',1),(156,'MW','MWI','454','MI','Malawi','Malawi',1),(157,'MX','MEX','484','MX','Mexico','Mexico',1),(158,'MY','MYS','458','MY','Malasia','Malaysia',1),(159,'MZ','MOZ','508','MZ','Mozambique','Mozambique',1),(160,'NA','NAM','516','WA','Namibia','Namibia',1),(161,'NC','NCL','540','NC','Nueva Caledonia','New Caledonia',1),(162,'NE','NER','562','NG','Níger','Niger',1),(163,'NF','NFK','574','NF','Norfolk Island','Norfolk Island',1),(164,'NG','NGA','566','NI','Nigeria','Nigeria',1),(165,'NI','NIC','558','NU','Nicaragua','Nicaragua',1),(166,'NL','NLD','528','NL','Países Bajos','Netherlands',1),(167,'NO','NOR','578','NO','Noruega','Norway',1),(168,'NP','NPL','524','NP','Nepal','Nepal',1),(169,'NR','NRU','520','NR','Nauru','Nauru',1),(170,'NU','NIU','570','NE','Niue','Niue',1),(171,'NZ','NZL','554','NZ','Nueva Zelanda','New Zealand',1),(172,'OM','OMN','512','MU','Omán','Oman',1),(173,'PA','PAN','591','PM','Panamá','Panama',1),(174,'PE','PER','604','PE','Perú','Peru',1),(175,'PF','PYF','258','FP','Polinesia Francés','French Polynesia',1),(176,'PG','PNG','598','PP','Papua Nueva Guinea','Papua New Guinea',1),(177,'PH','PHL','608','RP','Filipinas','Philippines',1),(178,'PK','PAK','586','PK','Pakistán','Pakistan',1),(179,'PL','POL','616','PL','Polonia','Poland',1),(180,'PM','SPM','666','SB','San Pedro y Miquelón','Saint Pierre and Miquelon',1),(181,'PN','PCN','612','PC','Islas Pitcairn','Pitcairn Island',1),(182,'PR','PRI','630','RQ','Puerto Rico','Puerto Rico',1),(183,'PS','PSE','275','GZ','Palestina','Palestinian territories',1),(184,'PT','PRT','620','PO','Portugal','Portugal',1),(185,'PW','PLW','585','PS','Palau','Palau',1),(186,'PY','PRY','600','PA','Paraguay','Paraguay',1),(187,'QA','QAT','634','QA','Qatar','Qatar',1),(188,'RE','REU','638','RE','Reunión','Réunion',1),(189,'RO','ROU','642','RO','Rumania','Romania',1),(190,'RS','SRB','688','RI','Serbia','Serbia',1),(191,'RU','RUS','643','RS','Rusia','Russian Federation',1),(192,'RW','RWA','646','RW','Ruanda','Rwanda',1),(193,'SA','SAU','682','SA','Arabia Saudita','Saudi Arabia',1),(194,'SB','SLB','090','BP','Islas Salomón','Solomon Islands',1),(195,'SC','SYC','690','SE','Seychelles','Seychelles',1),(196,'SD','SDN','729','SU','Sudán','Sudan',1),(197,'SE','SWE','752','SW','Suecia','Sweden',1),(198,'SG','SGP','702','SN','Singapur','Singapore',1),(199,'SH','SHN','654','SH','Santa Helena, Ascensión y Tristán da Cunha','Saint Helena, Ascension and Tristan da Cunha',1),(200,'SI','SVN','705','SI','Eslovenia','Slovenia',1),(201,'SJ','SJM','744','SV','Islas Svalbard y Jan Mayen','Svalbard and Jan Mayen',1),(202,'SK','SVK','703','LO','Eslovaquia','Slovakia',1),(203,'SL','SLE','694','SL','Sierra Leona','Sierra Leone',1),(204,'SM','SMR','674','SM','San Marino','San Marino',1),(205,'SN','SEN','686','SG','Senegal','Senegal',1),(206,'SO','SOM','706','SO','Somalia','Somalia',1),(207,'SR','SUR','740','NS','Suriname','Suriname',1),(208,'ST','STP','678','TP','Santo Tomé y Príncipe','Sao Tome and Principe',1),(209,'SV','SLV','222','ES','El Salvador','El Salvador',1),(210,'SX','SXM','534','NN','San Martín (parte Holandesa)','Sint Maarten (Dutch part)',1),(211,'SY','SYR','760','SY','Siria','Syrian Arab Republic',1),(212,'SZ','SWZ','748','WZ','Suazilandia','Swaziland',1),(213,'TC','TCA','796','TK','Islas Turcas y Caicos','Turks and Caicos Islands',1),(214,'TD','TCD','148','CD','Chad','Chad',1),(215,'TF','ATF','260','FS','Territorios Franceses del Sur','French Southern Territories',1),(216,'TG','TGO','768','TO','Togo','Togo',1),(217,'TH','THA','764','TH','Tailandia','Thailand',1),(218,'TJ','TJK','762','TI','Tayikistán','Tajikistan',1),(219,'TK','TKL','772','TL','Tokelau','Tokelau',1),(220,'TL','TLS','626','TT','Timor Oriental','Timor-Leste (East Timor)',1),(221,'TM','TKM','795','TX','Turkmenistán','Turkmenistan',1),(222,'TN','TUN','788','TS','Túnez','Tunisia',1),(223,'TO','TON','776','TN','Tonga','Tonga',1),(224,'TR','TUR','792','TU','Turquía','Turkey',1),(225,'TT','TTO','780','TD','Trinidad y Tobago','Trinidad and Tobago',1),(226,'TV','TUV','798','TV','Tuvalu','Tuvalu',1),(227,'TW','TWN','158','TW','Taiwan','Taiwan',1),(228,'TZ','TZA','834','TZ','Tanzania','Tanzania',1),(229,'UA','UKR','804','UP','Ucrania','Ukraine',1),(230,'UG','UGA','800','UG','Uganda','Uganda',1),(231,'UM','UMI','581',NULL,'Islas Ultramarinas de EE.UU.','United States Minor Outlying Islands',1),(232,'US','USA','840','US','Estados Unidos','United States',1),(233,'UY','URY','858','UY','Uruguay','Uruguay',1),(234,'UZ','UZB','860','UZ','Uzbekistán','Uzbekistan',1),(235,'VA','VAT','336','VT','Ciudad del Vaticano','Vatican City State',1),(236,'VC','VCT','670','VC','San Vicente y las Granadinas','Saint Vincent and the Grenadines',1),(237,'VE','VEN','862','VE','Venezuela','Venezuela',1),(238,'VG','VGB','092','VI','Islas Vírgenes Británicas','Virgin Islands, British',1),(239,'VI','VIR','850','VQ','Islas Vírgenes de EE.UU.','Virgin Islands, U.S.',1),(240,'VN','VNM','704','VM','Viet Nam','Viet Nam',1),(241,'VU','VUT','548','NH','Vanuatu','Vanuatu',1),(242,'WF','WLF','876','WF','Wallis y Futuna','Wallis and Futuna',1),(243,'WS','WSM','882','WS','Samoa','Samoa',1),(244,'YE','YEM','887','YM','Yemen','Yemen',1),(245,'YT','MYT','175','MF','Mayotte','Mayotte',1),(246,'ZA','ZAF','710','SF','Sudáfrica','South Africa',1),(247,'ZM','ZMB','894','ZA','Zambia','Zambia',1),(248,'ZW','ZWE','716','ZI','Zimbabue','Zimbabwe',1),(249,'SS','SSD','728','OD','Sudán del Sur','South Sudan',1);
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `idParticipante` int(11) NOT NULL AUTO_INCREMENT,
  `nombresParticipante` varchar(100) NOT NULL DEFAULT 'Sin Nombres',
  `apellidosParticipante` varchar(100) NOT NULL DEFAULT 'Sin Apellidos',
  `correoElectronicoParticipante` varchar(100) NOT NULL,
  `contrasenaParticipante` varchar(45) NOT NULL,
  `identificacionParticipante` varchar(20) NOT NULL,
  `institucionParticipante` varchar(100) DEFAULT NULL,
  `telefonoParticipante` varchar(15) DEFAULT NULL,
  `telefono2Participante` varchar(15) DEFAULT NULL,
  `fechaIngresoParticipante` datetime NOT NULL DEFAULT '1900-01-01 01:01:01',
  `estadoParticipante` tinyint(1) NOT NULL DEFAULT '1',
  `idPais` int(11) NOT NULL,
  `ciudadParticipante` varchar(100) DEFAULT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idDescuentoRegistrado` int(11) DEFAULT NULL,
  `documentoDescuento` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idParticipante`),
  KEY `fk_participante_pais1_idx` (`idPais`),
  KEY `fk_participante_tipoidentificacion1_idx` (`idTipoIdentificacion`),
  KEY `fk_participante_descuento` (`idDescuentoRegistrado`),
  CONSTRAINT `fk_participante_descuento` FOREIGN KEY (`idDescuentoRegistrado`) REFERENCES `descuento` (`idDescuento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participante_pais1` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participante_tipoidentificacion1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipoidentificacion` (`idTipoIdentificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participanteevento`
--

DROP TABLE IF EXISTS `participanteevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participanteevento` (
  `idParticipanteEvento` int(11) NOT NULL AUTO_INCREMENT,
  `fechaMatriculaParticipanteEvento` datetime NOT NULL DEFAULT '1900-01-01 01:01:01',
  `certificadoParticipanteEvento` varchar(100) DEFAULT NULL,
  `facturaCiespalParticipanteEvento` varchar(45) DEFAULT NULL,
  `entregaCertificadoParticipanteEvento` int(11) NOT NULL DEFAULT '0',
  `valorPagadoParticipanteEvento` float NOT NULL DEFAULT '0',
  `confirmadoPorParticipanteEvento` int(11) NOT NULL DEFAULT '0',
  `fechaConfirmadoPorParticipanteEvento` datetime DEFAULT NULL,
  `observacionesParticipanteEvento` varchar(100) DEFAULT NULL,
  `valorAPagar` float NOT NULL DEFAULT '0',
  `solapinParticipanteEvento` tinyint(1) NOT NULL DEFAULT '0',
  `rutaEvidenciaPago` varchar(50) DEFAULT NULL,
  `rutaCertificadoParticipanteEvento` varchar(100) DEFAULT NULL,
  `descripcionEvidenciaPago` varchar(150) DEFAULT NULL,
  `estadoParticipanteEvento` int(11) NOT NULL DEFAULT '2' COMMENT 'Se manejarán 3 estados: 2=Pendiente, 0=Eliminado (no confirmado), 1=Confirmado',
  `idValorEvento` int(11) DEFAULT NULL,
  `idTipoParticipante` int(11) NOT NULL,
  `idTipoFormaPago` int(11) NOT NULL,
  `idParticipante` int(11) NOT NULL,
  `idEvento` int(11) NOT NULL,
  `idDatosFacturacion` int(11) NOT NULL,
  PRIMARY KEY (`idParticipanteEvento`),
  KEY `fk_participanteevento_valorevento_idx` (`idValorEvento`),
  KEY `fk_participanteevento_tipoparticipante1_idx` (`idTipoParticipante`),
  KEY `fk_participanteevento_tipoformapago1_idx` (`idTipoFormaPago`),
  KEY `fk_participanteevento_participante1_idx` (`idParticipante`),
  KEY `fk_participanteevento_evento1_idx` (`idEvento`),
  KEY `fk_participanteevento_datosfacturacion1_idx` (`idDatosFacturacion`),
  CONSTRAINT `fk_participanteevento_datosfacturacion1` FOREIGN KEY (`idDatosFacturacion`) REFERENCES `datosfacturacion` (`idDatosFacturacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participanteevento_evento1` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participanteevento_participante1` FOREIGN KEY (`idParticipante`) REFERENCES `participante` (`idParticipante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participanteevento_tipoformapago1` FOREIGN KEY (`idTipoFormaPago`) REFERENCES `tipoformapago` (`idTipoFormaPago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participanteevento_tipoparticipante1` FOREIGN KEY (`idTipoParticipante`) REFERENCES `tipoparticipante` (`idTipoParticipante`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participanteevento_valorevento` FOREIGN KEY (`idValorEvento`) REFERENCES `valorevento` (`idValorEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participanteevento`
--

LOCK TABLES `participanteevento` WRITE;
/*!40000 ALTER TABLE `participanteevento` DISABLE KEYS */;
/*!40000 ALTER TABLE `participanteevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoempleado`
--

DROP TABLE IF EXISTS `tipoempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoempleado` (
  `idTipoEmpleado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionTipoEmpleado` varchar(45) NOT NULL DEFAULT '',
  `estadoTipoEmpleado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoEmpleado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoempleado`
--

LOCK TABLES `tipoempleado` WRITE;
/*!40000 ALTER TABLE `tipoempleado` DISABLE KEYS */;
INSERT INTO `tipoempleado` VALUES (1,'Normal',1),(2,'Administrador',1),(3,'Financiero',1);
/*!40000 ALTER TABLE `tipoempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoevento`
--

DROP TABLE IF EXISTS `tipoevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoevento` (
  `idTipoEvento` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionTipoEvento` varchar(45) NOT NULL DEFAULT 'Ninguna',
  `estadoTipoEvento` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoEvento`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoevento`
--

LOCK TABLES `tipoevento` WRITE;
/*!40000 ALTER TABLE `tipoevento` DISABLE KEYS */;
INSERT INTO `tipoevento` VALUES (1,'LANZAMIENTO DE LIBRO',1),(2,'FORO',1),(3,'CONVERSATORIO',1),(4,'CONGRESO',1),(5,'TALLER',1),(6,'CURSO',1);
/*!40000 ALTER TABLE `tipoevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoformapago`
--

DROP TABLE IF EXISTS `tipoformapago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoformapago` (
  `idTipoFormaPago` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionTipoFormaPago` varchar(45) NOT NULL DEFAULT 'Ninguna',
  `estadoTipoFormaPago` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoFormaPago`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoformapago`
--

LOCK TABLES `tipoformapago` WRITE;
/*!40000 ALTER TABLE `tipoformapago` DISABLE KEYS */;
INSERT INTO `tipoformapago` VALUES (1,'PAYPAL',2),(2,'DEPOSITO / TRANSFERECIA',1),(3,'EFECTIVO',1);
/*!40000 ALTER TABLE `tipoformapago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoidentificacion`
--

DROP TABLE IF EXISTS `tipoidentificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoidentificacion` (
  `idTipoIdentificacion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionTipoIdentificacion` varchar(45) NOT NULL DEFAULT 'CEDULA',
  `estadoTipoIdentificacion` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoIdentificacion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoidentificacion`
--

LOCK TABLES `tipoidentificacion` WRITE;
/*!40000 ALTER TABLE `tipoidentificacion` DISABLE KEYS */;
INSERT INTO `tipoidentificacion` VALUES (1,'CEDULA',1),(2,'PASAPORTE',1),(3,'RUC',1);
/*!40000 ALTER TABLE `tipoidentificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoparticipante`
--

DROP TABLE IF EXISTS `tipoparticipante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoparticipante` (
  `idTipoParticipante` int(11) NOT NULL AUTO_INCREMENT,
  `descripcionTipoParticipante` varchar(45) NOT NULL,
  `estadoTipoParticipante` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoParticipante`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoparticipante`
--

LOCK TABLES `tipoparticipante` WRITE;
/*!40000 ALTER TABLE `tipoparticipante` DISABLE KEYS */;
INSERT INTO `tipoparticipante` VALUES (1,'ESTUDIANTE',1),(2,'ASISTENTE GENERAL',1),(3,'PONENTE',1),(4,'DOCENTE',1),(5,'INVITADO',1),(6,'BECADO',1);
/*!40000 ALTER TABLE `tipoparticipante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `desplegar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valorevento`
--

DROP TABLE IF EXISTS `valorevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valorevento` (
  `idValorEvento` int(11) NOT NULL AUTO_INCREMENT,
  `costoValorEvento` float NOT NULL DEFAULT '0',
  `estadoValorEvento` tinyint(1) NOT NULL DEFAULT '1',
  `idTipoParticipante` int(11) NOT NULL,
  `idEvento` int(11) NOT NULL,
  PRIMARY KEY (`idValorEvento`),
  KEY `fk_valorevento_tipoparticipante1_idx` (`idTipoParticipante`),
  KEY `fk_valorevento_evento1_idx` (`idEvento`),
  CONSTRAINT `fk_valorevento_evento1` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_valorevento_tipoparticipante1` FOREIGN KEY (`idTipoParticipante`) REFERENCES `tipoparticipante` (`idTipoParticipante`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valorevento`
--

LOCK TABLES `valorevento` WRITE;
/*!40000 ALTER TABLE `valorevento` DISABLE KEYS */;
/*!40000 ALTER TABLE `valorevento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-02  3:00:59
