FROM php:7.3-apache
#RUN rm -rf /etc/apt/sources.list && touch /etc/apt/sources.list
#RUN sed -i '/deb.debian.org/s/stretch/buster/g' /etc/apt/sources.list
RUN apt-get install software-properties-common -y
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y \
        libzip-dev \
        zip \
        libpng-dev
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli && a2enmod rewrite && a2enmod headers
RUN docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip %% docker-php-ext-install gd
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer